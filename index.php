<?php
	get_header();
?>
<?php $pagination_num = (is_active_sidebar('side_panel')) ? 4 : 8; ?>
<main class="container main">
	<section class="filter_cnt">
		<div class="row">
			<div class="col-12 min">
				<div class="d-flex flex-row flex-wrap flex-lg-nowrap justify-content-around align-items-center sorting_post_cnt hidden">
					<?php
		                $categories = get_categories(array(
						    'orderby' => 'name',
						    'order'   => 'ASC'
						));
						$def_cat = array();
						foreach( $categories as $category ) {
							array_push($def_cat, $category->cat_ID);
						} 
						$list = array_map('strval', $def_cat);
						if(isset($_SESSION['cat_var_arr'])){
							$not_checked_categories = array_diff($list, $_SESSION['cat_var_arr']);
						}
		            ?>  
	                <div class="d-flex flex-row flex-wrap sorting_post_cnt__item">
						<div class="cat_title">CATEGORIES:</div>
		                <?php 
			                if(!isset($_SESSION['cat_var_arr'])){
				                foreach( $categories as $category ){
			               	    echo '<div class="d-flex justify-content-between align-items-center flex-row category_field"> 
									    <span class="pr-5 py-1 sorting_lable">' . $category->name . ':</span> <input type="checkbox" id="'. $category->slug .'" name="'. $category->name .'" value="'. $category->cat_ID .'" class="check_sort" checked> 
									  </div>'; 
								}
							}else{
								$new_check_box_arr = array();
								if(!empty($not_checked_categories)){
									$n_checked_cat = get_categories(array(
									    'orderby' => 'name',
									    'order'   => 'ASC',
									    'include' => $not_checked_categories
									));
									
									foreach( $n_checked_cat as $category ){	
					               	    $cb = '<div class="d-flex justify-content-between align-items-center flex-row category_field"> 
											    <span class="pr-5 py-1 sorting_lable">' . $category->name . ':</span> <input type="checkbox" id="'. $category->slug .'" name="'. $category->name .'" value="'. $category->cat_ID .'" class="check_sort"> 
											  </div>';
									    $new_check_box_arr[$category->slug] = $cb;
								    } 
                                }
                                if(!empty($_SESSION['cat_var_arr'])){
									$checked_cat = get_categories(array(
									    'orderby' => 'name',
									    'order'   => 'ASC',
									    'include' => $_SESSION['cat_var_arr']
									));
								
									foreach( $checked_cat as $category ){	
					               	    $cb = '<div class="d-flex justify-content-between align-items-center flex-row category_field"> 
											    <span class="pr-5 py-1 sorting_lable">' . $category->name . ':</span> <input type="checkbox" id="'. $category->slug .'" name="'. $category->name .'" value="'. $category->cat_ID .'" class="check_sort" checked> 
											  </div>';
										$new_check_box_arr[$category->slug] = $cb;	  
								    } 
								}
								ksort($new_check_box_arr);
								foreach ($new_check_box_arr as $value) {
									echo $value;
								}
							}
						?>
				    </div>
					
					<div class="d-flex justify-content-around justify-content-xl-end sorting_post_cnt__item">
						<button id="sort" data-url="<?php echo admin_url('admin-ajax.php'); ?>" class="btn">Sort and show
							<i class="sort_show">
								<svg class="icon icon-twoarrows"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-twoarrows"></use></svg>
							</i>
						</button>
					</div>
				</div>
			</div>
		</div>
		<button class="filter_cnt__button">Filter articles
			<i class="filter_button_icon">
				<svg class="icon icon-level-down"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-level-down"></use></svg>
			</i>
		</button>
	</section>
	<div class="row">

		<?php echo (is_active_sidebar('side_panel')) ? '<div class="overfl mh order-2 order-lg-1 col-12 col-lg-7 col-xl-8 position-relative">': '<div class="overfl mh col-12 position-relative">'; ?>
			<div class="posts_cnt">
				<div class="posts_cnt__loader_cnt">
					<div class="hidden_post_loader">
					    <div class="post_cnt_loader">
							<div class="post_w_thum_loader"></div>
							<div class="post_excerpt_loader"></div>
					    </div>
					    <div class="post_cnt_loader">
							<div class="post_header_loader"></div>
							<div class="post_excerpt_loader_no_thum"></div>
					    </div>
					    <div class="post_cnt_loader">
							<div class="post_w_thum_loader"></div>
							<div class="post_excerpt_loader"></div>
					    </div>
					    <div class="post_cnt_loader">
							<div class="post_header_loader"></div>
							<div class="post_excerpt_loader_no_thum"></div>
					    </div>
					    <div class="post_cnt_loader">
							<div class="post_w_thum_loader"></div>
							<div class="post_excerpt_loader"></div>
					    </div>
					</div>
				</div>
				<?php echo '<div data-number="1" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/' . if_paged() . '">'; ?>
					<?php 
					$cat = isset($_SESSION['cat_var_arr']) ? $_SESSION['cat_var_arr'] : $def_cat;
		        	$args = array(
		        		'post_type'   => 'post',
		        		'post_status' => 'publish',
		        		'paged'       => if_paged(1),
		        		'category__in'=> $cat
		        	);
		        	$m_query = new WP_Query($args);
					if ( $m_query->have_posts() && !empty($cat) ) {

						while ( $m_query->have_posts() ) {
							$m_query->the_post();					        
							get_template_part( 'template-parts/index', 'posts' ); 
				        	
						} // end while
						wp_reset_postdata();
					} else{
						echo '<h1 class="text-center mt-5 mb-3 no_results_found">'. esc_html__( "No results found", "cas_theme" ) .'</h1>';
					}// end if
					?>
					<?php if ( $m_query->max_num_pages > 1 && !empty($cat) ): ?>
                    <div class="pagination_cnt">
						<div class="pagination_cnt__inner" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
							<?php echo paginate_links(array(
								'base'               => '#',
								'format'             => '',
								'prev_next'          => true,
								'type'               => 'plain',
								'total'              => $m_query->max_num_pages,
								'end_size'           => 2,
								'mid_size'           => $pagination_num,
								));?>
						</div>
					</div> 
					<?php endif; ?>
				<?php echo '</div>'; ?>
			</div>
	    </div>
	    <?php if(is_active_sidebar('side_panel')): ?>
	    	<aside class="order-1 order-lg-2 col-12 col-lg-5 col-xl-4 aside_panel">
	    		<?php dynamic_sidebar('side_panel'); ?>
	    	</aside>
		<?php endif; ?>
    </div>
</main>

<?php		
	get_footer();
?>