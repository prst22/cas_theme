<?php
/**
 * Template part for displaying video posts
 *
 */
?>
<?php 
	$streamer_id = get_post_meta(get_the_ID(), '_streams_id_value_key', true); 
	$stream_name = trim(strtolower(esc_html(get_the_title())));
	$url = 'https://api.twitch.tv/helix/users?login='. $stream_name;
    $opts = [
	    "http" => [
	        "method" => "GET",
	        "header" => "Client-ID: vj6rb2kg6ft1vw2w0z42y6ysehddpv\r\n"
	    ]
	];
	$context = stream_context_create($opts);
	// Open the file using the HTTP headers set above
	$file = file_get_contents($url, false, $context);
    $encoded = json_decode($file);

?>

<article <?php post_class( array( 'single_post', 'row') ); ?>>
	<div class="col-12 mt-5 mb-md-3 position-relative main_post">
		<h4 class="pt-5 position-relative single_streams_page_heading">
			<i class="icon-tv">
				<svg class="icon icon-tv"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-tv"></use></svg>
			</i>
			<?php echo strtoupper(get_the_title()); ?>
		</h4>
		<div class="twitch_player_cnt">
			<div class="video_content">
				<script src= "https://player.twitch.tv/js/embed/v1.js"></script>
					<div id="twitch_player"></div>
					<script type="text/javascript">
					  var options = {
					    width: 854,
					    height: 480,
					    channel: "<?php echo $stream_name; ?>",
					  };
					  var player = new Twitch.Player("twitch_player", options);
				    </script>
			</div>
		</div>
		<?php if(strlen($encoded->data[0]->description) > 0): ?>
			<div class="pt-4 pt-lg-5 mb-5 mb-md-4 stream_description">
				<span class="mb-2 stream_description__lable">DESCRIPTION:</span><br>
				<?php echo $encoded->data[0]->description; ?>
			</div>
	    <?php endif; ?>
	</div>
</article>