<?php 
	$post_format = get_post_format();
	$fetch_pag = get_query_var('fetch_pages');
	$fetch_search = get_query_var('search_fetch_true');
	switch ($post_format) {

		case 'video':?>
			
			<article <?php post_class( 'index_post' ); ?>>
				<figure class="position-relative thumbnail_cnt video_thumbnail_cnt">
					<a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link video_thumbnail_cnt__link heading_w_thum">
						<h1 class="position-absolute index_post__heading index_post__heading--w_thum">
							<?php the_title(); ?>
						</h1>
						<?php if(has_post_thumbnail()): ?>
							<?php 
							if(is_search()){
								the_post_thumbnail('large-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumbnail'),
									'alt' => get_the_title()
								));
							}else if($fetch_search == true){
								the_post_thumbnail('large-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumbnail'),
									'alt' => get_the_title()
								));
							}else if(is_home() && is_active_sidebar('side_panel')){
								the_post_thumbnail('medium-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'medium-thumbnail'),
									'alt' => get_the_title()
								));
							}else if(is_active_sidebar('side_panel') && $fetch_pag == true){
								the_post_thumbnail('medium-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'medium-thumbnail'),
									'alt' => get_the_title()
								));
							}else{
								the_post_thumbnail('large-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumbnail'),
									'alt' => get_the_title()
								));
							} 
							?>
							<?php else: ?>
								<img src="<?php echo get_template_directory_uri() .'/inc/images/video_placeholder.png'; ?>" class="video_thumbnail thumbnail_img" data-src="<?php echo get_template_directory_uri() .'/inc/images/video_placeholder.png'; ?>" alt="<?php the_title(); ?>">
						<?php endif; ?>	
						<div class="video_play_btn">
							<span>PLAY</span>
							<i>
								<svg class="icon play-circle"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play-circle"></use></svg>
							</i>
						</div>
						<div class="image_loader_cnt">
							<div class="simple-spinner">
							</div>
						</div>
                    </a>   
				</figure> 
				<?php 
					$filtered_execr = str_replace("&nbsp;", "", get_the_excerpt());
					$filtered_execr = html_entity_decode($filtered_execr); 
	               ?>
				<div class="position-relative index_post__excerpt index_post__excerpt--w_thum <?php echo ($filtered_execr == '') ? 'pt-4 pb-4 video_excerpt' : 'video_excerpt'; ?> box_shadow">
					<div class="d-flex justify-content-between index_post__info index_post__info--video">
						<div title="Number of views" class="d-flex align-items-center">
							<span class="pr-1 com_inf">
								<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
							</span>
							<span class="com_inf--num">
								<?php 
									$views = get_post_meta(get_the_ID(), 'cas_theme_post_views', TRUE);
									$print_views = empty($views) ? 0 : $views;
									echo $print_views;
								?>
							</span>
						</div>
						<div title="Number of reviews" class="reviews">
							<?php if(function_exists('the_ratings')) { 
								echo '<span class="ratings_cnt">';
								the_ratings(); 
								echo '</span>';
							} ?>
							<span class="pr-1 com_inf">
								<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
							</span>
							<span class="com_inf--num">
								<?php echo get_comments_number(); ?>
							</span>
						</div>
					</div>
					<?php ($filtered_execr == '') ? null : the_excerpt(); ?>

				</div>
			</article>
			
		<?php break;

		case 'link':?>
			<article <?php post_class( array('index_post', 'index_link') ); ?>>

				<div class="d-flex justify-content-between index_post__info">
					<div title="Number of views" class="d-flex align-items-center">
						<span class="pr-1 com_inf">
							<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php 
								$views = get_post_meta(get_the_ID(), 'cas_theme_post_views', TRUE);
								$print_views = empty($views) ? 0 : $views;
								echo $print_views;
							?>
						</span>
					</div>
					<div title="Number of reviews" class="d-flex align-items-center">
						<span class="pr-1 com_inf">
							<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php echo get_comments_number(); ?>
						</span>
					</div>
				</div>

				<h1 class="index_post__heading d-flex justify-content-center align-items-center">
					<a href="<?php echo wp_strip_all_tags(get_the_content()); ?>" target="_blank" title="Link to <?php the_title_attribute(); ?>" class="d-flex flex-column align-items-center">
						<span><?php the_title(); ?></span>
						<span class="d-inline-block mt-5 index_link__icon">
							<svg class="icon link"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#link"></use></svg>
						</span>
					</a>
				</h1>
				<a href="<?php the_permalink(); ?>" title="Link to comments" class="link">
					View comments
				</a>
			</article>
		<?php break;

		case 'quote':?>
			<article <?php post_class( array('index_post', 'quotes_post') ); ?>>
				<div class="d-flex justify-content-between index_post__info">
					<div title="Number of views" class="d-flex align-items-center">
						<span class="pr-1 com_inf">
							<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php 
								$views = get_post_meta(get_the_ID(), 'cas_theme_post_views', TRUE);
								$print_views = empty($views) ? 0 : $views;
								echo $print_views;
							?>
						</span>
					</div>
					<div title="Number of reviews" class="d-flex align-items-center">
						<span class="pr-1 com_inf">
							<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php echo get_comments_number(); ?>
						</span>
					</div>
				</div>
				<div class="quotes_post__inner">
					<span class="quotes_post quotes_post--quote_left">
						<svg class="icon icon-quotes-left"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-quotes-left"></use></svg>
					</span>	

					<div class="my-5 quotes_post__content">
						<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_content(); ?>
						</a>
					</div>
					
					<span class="quotes_post quotes_post--quote_right">
						<svg class="icon icon-quotes-right"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-quotes-right"></use></svg>
					</span>	
				</div>
			</article>
		<?php break;

		default:

		if(get_post_type(get_the_ID()) === 'streams'): ?>
            <article <?php post_class( array('index_post') ); ?>>
	        	<h1 class="index_post__heading streams">
	        		<a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?> stream" class="stream_logo">
	        			<i class="icon-tv">
							<svg class="icon icon-tv"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-tv"></use></svg>
						</i>
	        		</a>
					<a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?> stream">
						
						<?php the_title(); ?> stream
					</a>
				</h1>
	        </article>
		    <?php else: ?>
			<article <?php post_class( array('index_post') ); ?>>
				<?php if(has_post_thumbnail()): ?>
				<figure class="position-relative thumbnail_cnt">
					<a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link video_thumbnail_cnt__link heading_w_thum">
						<h1 class="position-absolute index_post__heading index_post__heading--w_thum">
							<?php the_title(); ?>
						</h1>
						<?php
							if(is_search()){
								the_post_thumbnail('large-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumbnail'),
									'alt' => get_the_title()
								));
							}else if($fetch_search == true){
								the_post_thumbnail('large-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumbnail'),
									'alt' => get_the_title()
								));
							}else if(is_home() && is_active_sidebar('side_panel')){
								the_post_thumbnail('medium-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'medium-thumbnail'),
									'alt' => get_the_title()
								));
							}else if(is_active_sidebar('side_panel') && $fetch_pag == true){
								the_post_thumbnail('medium-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'medium-thumbnail'),
									'alt' => get_the_title()
								));
							}else{
								the_post_thumbnail('large-thumbnail', 
								$attr = array(
									'class' => "video_thumbnail thumbnail_img",
									'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumbnail'),
									'alt' => get_the_title()
								));
							} 
							?>
						
						<div class="image_loader_cnt">
							<div class="simple-spinner">
							</div>
						</div>
                    </a>   
				</figure> 
				
				<div class="position-relative index_post__excerpt index_post__excerpt--w_thum box_shadow">
					<div class="d-flex justify-content-between index_post__info index_post__info--w_thum">
						<div title="Number of views" class="d-flex align-items-center">
							<span class="pr-1 com_inf">
								<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
							</span>
							<span class="com_inf--num">
								<?php 
									$views = get_post_meta(get_the_ID(), 'cas_theme_post_views', TRUE);
									$print_views = empty($views) ? 0 : $views;
									echo $print_views;
								?>
							</span>

						</div>
						
						<div title="Number of reviews" class="reviews">
							<?php if(function_exists('the_ratings')) { 
								echo '<span class="ratings_cnt">';
								the_ratings(); 
								echo '</span>';
							} ?>
							<span class="pr-1 com_inf">
								<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
							</span>
							<span class="com_inf--num">
								<?php echo get_comments_number(); ?>
							</span>
						</div>
					</div>
					<?php the_excerpt(); ?>
				</div>
				<?php else: ?>
				<h1 class="index_post__heading">
					<a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</h1>
				<div class="d-flex justify-content-between index_post__info index_post__info">
					<div title="Number of views" class="d-flex align-items-center">
						<span class="pr-1 com_inf">
							<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php 
								$views = get_post_meta(get_the_ID(), 'cas_theme_post_views', TRUE);
								$print_views = empty($views) ? 0 : $views;
								echo $print_views;
							?>
						</span>
					</div>
					<div title="Number of reviews" class="reviews">
						<?php if(function_exists('the_ratings')) { 
							echo '<span class="ratings_cnt">';
							the_ratings(); 
							echo '</span>';
						} ?>
						<span class="pr-1 com_inf">
							<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php echo get_comments_number(); ?>
						</span>
					</div>
				</div>
				
				<div class="position-relative index_post__excerpt box_shadow">
					<?php the_excerpt(); ?>
				</div>
				<?php endif; ?>
			</article>
			<?php endif; ?>
		<?php break;
	}
	