<?php
/**
 * Template part for displaying video posts
 *
 */
?>
<article <?php post_class( array( 'single_post', 'row') ); ?>>
	<div class="col-12 mt-5 mb-md-3 position-relative main_post">
        <div class="information information--for_video position-absolute">
		    	<div class="information__inner">
	                <div class="information_item">
				    	<span class="com_inf">
							<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
						</span>
						<span class="com_inf com_inf--num">
							<?php 
								$views1 = get_post_meta(get_the_ID(), 'cas_theme_post_views', TRUE);
								$print_views1 = empty($views1) ? 0 : $views1;
								echo $print_views1;
							?>
						</span>
	                </div>
	                <div class="information_item">
				    	<span class="com_inf">
							<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php echo get_comments_number(); ?>
						</span>
					</div>
				    	<?php if(function_exists('the_ratings')) { 
			                echo '<span class="ratings_cnt">';
			                the_ratings(); 
			                echo '</span>';
			            } ?>
	            </div>
		    </div>
		<h1 class="pb-0 pt-5 position-relative">
			<?php the_title(); ?>

		</h1>

		<?php the_content(); ?>
	</div>
	
</article>