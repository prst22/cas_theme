<?php
/**
 * Template part for displaying link posts
 *
 */
?>
<article <?php post_class( array( 'single_post', 'row', 'single_link' ) ); ?>>

	<div class="col-12 mt-5 mb-3 main_post">
		<h1><?php the_title(); ?></h1>
		<p>
			<a href="<?php echo wp_strip_all_tags(get_the_content()); ?>" class="single_link__link">
				<?php echo wp_strip_all_tags(get_the_content()); ?>
			</a>
		</p>
	</div>
	
</article>