<?php
/**
 * Template part for displaying posts
 *
 */
?>
<article <?php post_class( array( 'single_post', 'row' ) ); ?>>

	<div class="col-12 mt-5 mb-md-3 pt-5">
		 
		<?php if(has_post_thumbnail()): ?>
		    <div class="single_picture position-relative">
			    <div class="information position-absolute">
			    	<div class="information__inner">
	                    <div class="information_item">
					    	<span class="com_inf">
								<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
							</span>
							<span class="com_inf com_inf--num">
								<?php 
									$views = get_post_meta(get_the_ID(), 'cas_theme_post_views', TRUE);
									$print_views = empty($views) ? 0 : $views;
									echo $print_views;
								?>
							</span>
	                    </div>
	                    <div class="information_item">
					    	<span class="com_inf">
								<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
							</span>
							<span class="com_inf--num">
								<?php echo get_comments_number(); ?>
							</span>
						</div>
					    	<?php if(function_exists('the_ratings')) { 
				                echo '<span class="ratings_cnt">';
				                the_ratings(); 
				                echo '</span>';
				            } ?>
                    </div>
			    </div>
				<?php the_post_thumbnail('large-thumbnail', 
					$attr = array(
						'class' => "single_post_thumbnail",
						'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumbnail'),
						'alt' => get_the_title()
				)); ?>
				<div class="image_loader_cnt image_loader_cnt--left">
					<div class="simple-spinner">
					</div>
				</div>
			</div>
			<div class="single_page_p single_page_p--w_picture box_shadow box_shadow--sin_pos main_post">
				<h1 class="single_title"><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		<?php else: ?>
			<div class="single_page_p single_page_p--no_picture box_shadow box_shadow--sin_pos main_post">
				<div class="information position-absolute">
			    	<div class="information__inner">
	                    <div class="information_item">
					    	<span class="com_inf">
								<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
							</span>
							<span class="com_inf com_inf--num">
								<?php 
									$views = get_post_meta(get_the_ID(), 'cas_theme_post_views', TRUE);
									$print_views = empty($views) ? 0 : $views;
									echo $print_views;
								?>
							</span>
	                    </div>
	                    <div class="information_item">
					    	<span class="com_inf">
								<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
							</span>
							<span class="com_inf--num">
								<?php echo get_comments_number(); ?>
							</span>
						</div>
					    	<?php if(function_exists('the_ratings')) { 
				                echo '<span class="ratings_cnt">';
				                the_ratings(); 
				                echo '</span>';
				            } ?>
                    </div>
			    </div>
				<h1 class="single_title"><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		<?php endif;?>
	</div>
	
</article>

