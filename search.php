<?php
/**
 * The template for displaying search results pages
 */

get_header(); ?>
<?php
	global $wp_query;
	$total_results = $wp_query->found_posts;
?>
<main class="container main search_results_page">
    <div class="row">
        <div class="col-12 mh">

            <header>
                <h1 class="mt-5 search_header">
					<?php printf( __( 'Search Results for: %s', 'cas_theme' ), get_search_query() ); ?>
					<?php echo $total_results; ?>
				</h1>
			</header>

			<div class="search_results_page__form"><?php get_search_form(); ?></div>

			<div class="results single_page">

				<div class="posts_cnt__loader_cnt">
					<div class="hidden_post_loader">
					    <div class="post_cnt_loader">
							<div class="post_w_thum_loader"></div>
							<div class="post_excerpt_loader"></div>
					    </div>
					    <div class="post_cnt_loader">
							<div class="post_header_loader"></div>
							<div class="post_excerpt_loader_no_thum"></div>
					    </div>
					    <div class="post_cnt_loader">
							<div class="post_w_thum_loader"></div>
							<div class="post_excerpt_loader"></div>
					    </div>
					    <div class="post_cnt_loader">
							<div class="post_header_loader"></div>
							<div class="post_excerpt_loader_no_thum"></div>
					    </div>
					    <div class="post_cnt_loader">
							<div class="post_w_thum_loader"></div>
							<div class="post_excerpt_loader"></div>
					    </div>
					</div>
				</div>
				
				<?php if ( have_posts() ) : ?>

						<!-- .page-header -->

					<?php echo '<div data-number="1" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/'. if_paged() .'?s='. get_search_query() .'&submit='.'">'; 
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/index', 'posts' ); 
					endwhile;
                        echo '<div class="pagination_cnt pagination_cnt--search">';
						// echo '<div class="rocbot"></div>';
							echo '<div class="pagination_cnt__inner" data-number="'.if_paged(1).'" data-url="'.admin_url('admin-ajax.php').'" data-search="'.get_search_query().'">';
									echo paginate_links(array(
										'base'               => '%_%',
										'format'             => '',
										'prev_next'          => false,
										'show_all'           => true,
										'type'               => 'plain',
										'current'            => max( 1, get_query_var('paged')),
										'end_size'           => 2,
										'mid_size'           => 8
										));
							echo '</div>';
						echo '</div>';
					echo '</div>';
							
					else :
						echo '<div data-number="1" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/'. if_paged() .'?s='. get_search_query() .'&submit='.'">'; 
						echo '<h1 class="text-center mt-5 mb-3 no_results_found">'. esc_html__( "No results found", "cas_theme" ) .'</h1>';
						echo '</div>';
					endif;
				?>

	        </div>
		        
		
		</div>
	</div>
</main>
<?php get_footer();
