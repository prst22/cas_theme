<?php

get_header(); ?>
    <main class="container main single_post_main">
        
        <?php
        // Start the loop.

        while ( have_posts() ) : the_post();
            save_post_views(get_the_ID());
            echo '<div class="single_post_main__inner">';
            get_template_part( 'template-parts/content', get_post_format() );
            echo '</div>';
 
            // If comments are open or we have at least one comment, load up the comment template.
            ?>
            <div class="row">
                    <div class="col-12 pt-md-3">
                        <?php 
                        // Previous/next post navigation.
                            the_post_navigation( array(
                                'next_text' => '<span class="meta-nav" aria-hidden="true" hidden>' . __( 'Next', 'cas_theme' ) . '</span> ' .
                                    '<span class="screen-reader-text">' . __( 'Next', 'cas_theme' ) . '</span> ' .
                                    '<svg class="pl-2 icon icon-chevron-right"><use xlink:href="'. get_template_directory_uri() . '/symbol-defs.svg#icon-chevron-right"></use></svg>',
                                'prev_text' => '<span class="meta-nav" aria-hidden="true" hidden>' . __( 'Previous', 'cas_theme' ) . '</span> ' .
                                    '<span class="screen-reader-text"><svg class="pr-2 icon icon-chevron-left"><use xlink:href="'. get_template_directory_uri() . '/symbol-defs.svg#icon-chevron-left"></use></svg>' . __( 'Previous', 'cas_theme' ) . '</span>',
                                'screen_reader_text' => 'Articles navigation',
                            ) );
                        ?>
                    </div>
            </div>
            <?php
            if ( comments_open() ) :
                comments_template();
            endif;
            
        // End the loop.
        endwhile;
        ?>
    </main><!-- .site-main -->
<?php get_footer(); ?>