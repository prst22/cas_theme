import jump from 'jump.js';
import Scrollbar from 'smooth-scrollbar';
import OverscrollPlugin from 'smooth-scrollbar/plugins/overscroll';
// blazy end
let ScrollbarOptions = {}, 
mainCntScroll,
postReveal = {},
lastScroll = 0,
contactForm = document.getElementById('one_column_cf'),
searchOpenBtn = document.querySelectorAll('a[href^="#open_search"]'),
searchCloseBtn = document.getElementById('close_search'),
searchBlurOverlay = document.querySelector('.search_blur_overlay'),
searchCnt = document.getElementById('search_block'),
mobileMenuToggle = document.getElementById('mobile_menu_btn'),
mobileMenuClose = document.getElementById('close_menu'),
iconCross = document.getElementsByClassName('cross'),
mobileMenu = document.querySelector('.mobile_menu_cnt'),
scrollbarMenuCnt = document.querySelector('.mobile_menu_cnt__inner'),
goTopBtn = document.querySelector('.go_top__icon'),
menuColumn,
scrl = document.getElementById('go'),
paginationLinks = document.querySelectorAll('.page-numbers:not(.current)'),
pagCnt = document.getElementsByClassName('pagination_cnt__inner')[0],
btn = document.querySelectorAll('.pagination_cnt__inner>a.page-numbers'),
searchPagination = document.querySelector('.pagination_cnt--search'),
sortButton = document.getElementById('sort'),
filter = document.querySelector('.sorting_post_cnt'),
filterButton = document.querySelector('.filter_cnt__button'),
siteContentWrap = document.querySelector('.main'),
bLazy,
bLazySinglePage,
bLazyVideo,
currentPage,
menuScrollBar,
throttledScrollArrowBtn,
scrollTarget = document.body.classList.contains('home') ? document.querySelector('.posts_cnt') : document.querySelector('.top'),
svgIcons = document.querySelectorAll('.svg_widget_number>svg'),
postPage = document.getElementsByClassName('post_item');
Scrollbar.use(OverscrollPlugin);
if(mobileMenu) {
	menuColumn = mobileMenu.querySelector('.mobile_menu_cnt__inner');
}
window.addEventListener("DOMContentLoaded", () =>{
  let firstPage = document.querySelector('div[data-number^="1"]:not(.added)');
  if(document.body.classList.contains('home') 
    ||  document.body.classList.contains('search')){
    firstPage.classList.add('added');
  }
  if(document.body.classList.contains('single')){
    document.querySelector('.single_post:not(.added)').classList.add('added');
  }
  if(document.body.classList.contains('page')){
    let page = document.querySelector('.single_page_to_animate:not(.revealed)');
    if(page) page.classList.add('revealed');
  }
  if(document.body.classList.contains('home') && svgIcons.length != 0){
    Array.from(svgIcons).forEach((element)=>{
        new Vivus(element.getAttribute('id'), {
            type: 'sync',
            duration: 120,
            animTimingFunction: Vivus.EASE_OUT,
            delay: 20
        });
    });
  }
});
window.addEventListener("load",() =>{
  
	if(document.body.classList.contains('home') 
		||  document.body.classList.contains('search')){
		
		for(let i = 0; i < btn.length; i++){
			btn[i].addEventListener('click', postReveal.requestPosts);
		}
    if(filterButton){
      filterButton.addEventListener('click', ()=>{
        if(filter.classList.contains('hidden')){
          filter.classList.remove('hidden');
          filter.classList.add('show');
        }else{
          filter.classList.remove('show');
          filter.classList.add('hidden');
        }
      });
    }
    if(sortButton){
      sortButton.addEventListener('click', postReveal.requestPosts);
    }
		
	}
	
  // initialize blazy start
  bLazy = new Blazy({
    validateDelay: 10,
    selector: '.thumbnail_img',
    success: ele => {
      let parentCnt = ele.parentNode;
      if(parentCnt.querySelector('.image_loader_cnt')){
        parentCnt.removeChild(parentCnt.querySelector('.image_loader_cnt'));
      } 
    },
    error: (ele, msg)=>{
      if(msg === 'missing'){
        let parentCnt = ele.parentNode;
        if(parentCnt.querySelector('.image_loader_cnt')){
          parentCnt.querySelector('.image_loader_cnt').innerHTML = 'failed to load';
        } 
      }else if(msg === 'invalid'){
        let parentCnt = ele.parentNode;
        if(parentCnt.querySelector('.image_loader_cnt')){
          parentCnt.querySelector('.image_loader_cnt').innerHTML = 'failed to load';
        } 
      } 
    }
  });
  
  bLazySinglePage = new Blazy({
    selector: '.single_post_thumbnail',
    success: ele => {
      let parentCnt = ele.parentNode;
      if(parentCnt.querySelector('.image_loader_cnt')){
        parentCnt.removeChild(parentCnt.querySelector('.image_loader_cnt'));
      } 
    },
    error: (ele, msg)=>{
      if(msg === 'missing'){
        let parentCnt = ele.parentNode;
        if(parentCnt.querySelector('.image_loader_cnt')){
          parentCnt.querySelector('.image_loader_cnt').innerHTML = 'failed to load';
        } 
      }else if(msg === 'invalid'){
        let parentCnt = ele.parentNode;
        if(parentCnt.querySelector('.image_loader_cnt')){
          parentCnt.querySelector('.image_loader_cnt').innerHTML = 'failed to load';
        } 
      } 
    }
  });
  // initialize blazy end
  
  if(document.body.classList.contains('videos_single_page')){
    bLazyVideo = new Blazy({
      selector: '.thumbnail_img_v_p',
      success: ele => {
        let parentCnt = ele.parentNode;
        if(parentCnt.querySelector('.image_loader_cnt')){
          parentCnt.removeChild(parentCnt.querySelector('.image_loader_cnt'));
        } 
      },
      error: (ele, msg)=>{
        if(msg === 'missing'){
          let parentCnt = ele.parentNode;
          if(parentCnt.querySelector('.image_loader_cnt')){
            parentCnt.querySelector('.image_loader_cnt').innerHTML = 'failed to load';
          } 
        }else if(msg === 'invalid'){
          let parentCnt = ele.parentNode;
          if(parentCnt.querySelector('.image_loader_cnt')){
            parentCnt.querySelector('.image_loader_cnt').innerHTML = 'failed to load';
          } 
        } 
      }
    });
  }
});
// post reveal start
postReveal.appendLoader = function(){
	let loader = document.createElement('DIV'),
	spinner = document.createElement('DIV');
	loader.classList.add('loading','loading--bottom');
	spinner.classList.add('simple-spinner');
	loader.appendChild(spinner);
	return loader;
},
postReveal.failed = function(){
  let failedCnt = document.createElement('DIV');
  failedCnt.innerHTML = 'Failed to load, pls refresh the page';
  failedCnt.classList.add('rocbot');
  return failedCnt;
},
postReveal.updatePagination = function (){
  let pagLinks = document.querySelectorAll('.pagination_cnt__inner>a.page-numbers');
    for(let i = 0; i < pagLinks.length; i++){
      pagLinks[i].addEventListener('click', postReveal.requestPosts);
    }
},
postReveal.requestPosts = function(event){
    event.preventDefault();
    let targetPaginationPage = (!isNaN(Number(event.target.textContent))) ? Number(event.target.textContent) : 1,
    search,
    lg = window.matchMedia("(min-width: 992px)"),
    offsetAmount = (document.body.classList.contains('home') && lg.matches) ? -96 : -48,
    dataNumber = targetPaginationPage,
    postsCnt = document.querySelector('.posts_cnt') || document.querySelector('.results'),
    bottomNotification = document.querySelector('.rocbot'),
    cat = document.getElementsByClassName('check_sort'),
    formData = new FormData();
    const ajaxUrl = pagCnt.getAttribute('data-url');
    if(searchPagination !== null){
        search = document.querySelector('.pagination_cnt--search>.pagination_cnt__inner').getAttribute('data-search');
        formData.append('search', search);
    }else{
      search = 0;
      formData.append('search', search);
      let checkedCategory = Array.from(cat).filter((checkedCat, index) =>{
      if(checkedCat.checked == true){
        return checkedCat;
      }
      });
      if(checkedCategory.length != 0){
        checkedCategory.forEach((catToSend, index)=>{
          formData.append(catToSend.getAttribute('id'), catToSend.getAttribute('value'));
        });
      }
  }
  
  let postloaderContent = `
  <div class="hidden_post_loader">
    <div class="post_cnt_loader">
     <div class="post_w_thum_loader"></div>
     <div class="post_excerpt_loader"></div>
    </div>
    <div class="post_cnt_loader">
     <div class="post_header_loader"></div>
     <div class="post_excerpt_loader_no_thum"></div>
    </div>
    <div class="post_cnt_loader">
     <div class="post_w_thum_loader"></div>
     <div class="post_excerpt_loader"></div>
    </div>
    <div class="post_cnt_loader">
     <div class="post_header_loader"></div>
     <div class="post_excerpt_loader_no_thum"></div>
    </div>
  </div>
  `,
  postLoader = document.createElement('DIV');
	formData.append('number', dataNumber);
	formData.append('action', 'request_posts');
  let currentPageElement = document.querySelector('.pagination_cnt>.pagination_cnt__inner>.current'), 
  options = {
  	method: 'POST',
  	body: formData
  },
  req = new Request(ajaxUrl, options);
  if(currentPageElement) currentPage = currentPageElement.textContent;
  postsCnt.innerHTML = '';
  postLoader.innerHTML = postloaderContent;
  postsCnt.appendChild(postLoader);
  postsCnt.appendChild(postReveal.appendLoader());
  let promiseWithNewChunkOfPosts = (function(){
    let data = fetch(req);
      return data;
    })(); 
  jump_module(scrollTarget, {
    duration: 1000,
    offset: offsetAmount,
    callback: () => {
      promiseWithNewChunkOfPosts
      .then((response)=>{
        if(response.ok){
          return response.text();
        }else{
          throw new Error('Bad HTTP');  
        }
      })
      .then((text)=>{
        if(text !== ''){
          let newPageWithPosts = document.createElement('DIV');
          newPageWithPosts.innerHTML = text;
          let newPosts = newPageWithPosts.firstChild;
          //clear previous page for new posts 
          postsCnt.innerHTML = '';
          postsCnt.appendChild(newPosts);
          pagCnt.setAttribute('data-number', Number(dataNumber));
          
          if(currentPageElement){
            document.body.classList.remove("paged-" + currentPage);
            document.body.classList.add("paged", "paged-" + targetPaginationPage);
          }
          updateUrl(newPosts);
          if(pagCnt){
            postReveal.updatePagination();
          }
          return postsCnt
        }
      })
      .then((newPostsAdded)=>{
        let elementToAnimate = newPostsAdded.querySelector('.post_item:not(.added)');
        if(postsCnt.parentNode.querySelector('.loading')) postsCnt.parentNode.removeChild(postsCnt.parentNode.querySelector('.loading'));
        return {
          newPosts: newPostsAdded,
          element: elementToAnimate
        };
      }) 
      .then((obj)=>{
        if(obj.newPosts){
            let promise = new Promise((resolve, reject) => {
                setTimeout(() => {
                    obj.element.classList.add('added');
                    resolve();
                }, 100);
            });
            promise.then(
                result => {
                    bLazy.load(document.querySelectorAll('.thumbnail_img'), true);
                },
                error => {
                    console.log("Rejected: " + error);
                }
            );
        }else{
            postsCnt.appendChild(postReveal.failed());
            setTimeout(()=>{
                postsCnt.querySelector('.rocbot').classList.add('reveal'); 
            }, 100);
        }
      })
      .catch((err) =>{
        if(err.message){
          console.log(err.message);
          if(postsCnt.querySelector('.loading')) postsCnt.removeChild(postsCnt.querySelector('.loading'));
          postsCnt.appendChild(postReveal.failed());
          setTimeout(()=>{
            postsCnt.querySelector('.rocbot').classList.add('reveal'); 
          }, 100);        
        }else{
          console.log(err);
        }
      });   
    }
  });
}

// post reveal end
function updateUrl(element){
	history.replaceState(null, null, element.getAttribute('data-page'));
	return false;
}

// underscore js part start
let o = function(obj) {
  if (obj instanceof o) return obj;
  if (!(this instanceof o)) return new o(obj);
  this.owrapped = obj;
};
o.now = Date.now || function() {
  return new Date().getTime();
};
o.throttle = function(func, wait, options) {
  let timeout, context, args, result;
  let previous = 0;
  if (!options) options = {};

  let later = function() {
    previous = options.leading === false ? 0 : o.now();
    timeout = null;
    result = func.apply(context, args);
    if (!timeout) context = args = null;
  };

  let throttled = function() {
    let now = o.now();
    if (!previous && options.leading === false) previous = now;
    let remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0 || remaining > wait) {
      if (timeout) {
      clearTimeout(timeout);
      timeout = null;
      }
      previous = now;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };

  throttled.cancel = function() {
    clearTimeout(timeout);
    previous = 0;
    timeout = context = args = null;
  };

  return throttled;
};
// underscore js part end
throttledScrollArrowBtn = o.throttle(showHideGoTopBtn, 300, {leading: true});

function showHideGoTopBtn(e){
	let currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
	if(Math.round(currentScroll) > Math.round(window.innerHeight)){
		goTopBtn.classList.remove('hidden_g');
	}else{
		goTopBtn.classList.add('hidden_g');
	}
}
document.addEventListener('scroll', throttledScrollArrowBtn);
// scroll to top button start

goTopBtn.addEventListener('click', ()=>{
  jump_module('.top', {
    duration: 1200
  });
});

// scroll to top button end

// contact form start
if(contactForm){
	contactForm.addEventListener('submit', function(e){
		e.preventDefault();
		sendContactInfo(e);
	})
}
// contact form end
 
function sendContactInfo(event){
	const ajaxUrl = event.target.getAttribute('data-url');
	let form = event.target,
	name = document.getElementById('oc_name'),
	email = document.getElementById('oc_email'),
	message = document.getElementById('oc_message'),
	btn = form.querySelector('[type="submit"]'),
	formData = new FormData();
	formData.append('name', name.value);
	formData.append('email', email.value);
	formData.append('message', message.value);
	formData.append('action', 'cas_save_contact');
  if( name.value === '' ){
  	name.classList.add('has_error');
  	name.nextElementSibling.classList.add('text_danger');
		return;
	}else{
		name.classList.remove('has_error');
		name.nextElementSibling.classList.remove('text_danger');
	}

	if( email.value === '' ){
		email.classList.add('has_error');
		email.nextElementSibling.classList.add('text_danger');
		return;
	}else{
		email.classList.remove('has_error');
		email.nextElementSibling.classList.remove('text_danger');
	}

	if( message.value === '' ){
		message.classList.add('has_error');
		message.nextElementSibling.classList.add('text_danger');
		return;
	}else{
		message.classList.remove('has_error');
		message.nextElementSibling.classList.remove('text_danger');
	}

  name.disabled = true;
  email.disabled = true;
  message.disabled = true;
  btn.disabled = true;
  let options = {
  	method: 'POST',
  	body: formData
  };
  document.querySelector('.text_info').classList.add('js_form_submission');
	let req = new Request(ajaxUrl, options);
    
	fetch(req)
		.then((response)=>{
			if(response.ok){
				setTimeout(function(){
					document.querySelector('.text_info').classList.remove('js_form_submission');
					document.querySelector('.text_success').classList.add('js_form_success');
					form.reset();
				}, 1000);
			}else{
				throw new Error('Bad HTTP');	
			}
		})
		.catch((err) =>{
			if(err.message){
	        name.disabled = false;
			    email.disabled = false;
			    message.disabled = false;
			    document.querySelector('.text_info').classList.remove('js_form_submission');
			    document.querySelector('.text_err').classList.add('js_form_error');
			}else{
				name.disabled = false;
			    email.disabled = false;
			    message.disabled = false;
			    document.querySelector('.text_info').classList.remove('js_form_submission');
			    document.querySelector('.text_err').classList.add('js_form_error');	
			}
		});

}
// open close search start
if(searchOpenBtn){
	
	searchCloseBtn.addEventListener('click', (e) =>{
    document.body.classList.toggle('search_active', !document.body.classList.contains('search_active'));
    searchCnt.classList.toggle('hidden_search', !searchCnt.classList.contains('hidden_search'));
    document.querySelector('.search_animate.revealed').classList.remove('revealed');
    document.removeEventListener('keydown', closeSearchOnEsc);
	});

	for (let i = 0; i < searchOpenBtn.length; i++) {
		searchOpenBtn[i].addEventListener('click', (e) =>{
			e.preventDefault();
      document.body.classList.toggle('search_active', !document.body.classList.contains('search_active'));
      searchCnt.classList.toggle('hidden_search', !searchCnt.classList.contains('hidden_search'));
      document.querySelector('.search_animate:not(.revealed)').classList.add('revealed');
      document.addEventListener('keydown', closeSearchOnEsc);
		});
	}
}

function closeSearchOnEsc (evt){
    evt = evt || window.event;
    let isEscape = false;
    if("key" in evt){
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    }else{
        isEscape = (evt.keyCode == 27);
    }
    if(isEscape){
      document.body.classList.remove('search_active');
      searchCnt.classList.add('hidden_search');
      document.querySelector('.search_animate.revealed').classList.remove('revealed');
      document.removeEventListener('keydown', closeSearchOnEsc);
    } 
}
// open close search end

// mobile menu start
if(mobileMenuToggle) mobileMenuToggle.addEventListener('click', openCloseMenu);
if(mobileMenuClose) mobileMenuClose.addEventListener('click', openCloseMenu);

function openCloseMenu(e){
	e.stopPropagation();
	if(menuColumn.classList.contains('column_hidden')){
		mobileMenu.classList.remove('hidden');
		menuColumn.classList.remove('column_hidden');
    document.body.classList.add('m_menu_active');
    if(siteContentWrap){
      siteContentWrap.classList.add('offset');
    }
	}else{
		mobileMenu.classList.add('hidden');
		menuColumn.classList.add('column_hidden');
    document.body.classList.remove('m_menu_active');
    if(siteContentWrap){
      siteContentWrap.classList.remove('offset');
    }
	}
}
if(scrollbarMenuCnt){
	menuScrollBar = Scrollbar.init(scrollbarMenuCnt, 
	    ScrollbarOptions = {
	    alwaysShowTracks: true
	});
}

// close icons animation start
window.addEventListener("DOMContentLoaded", () =>{
if(iconCross.length > 0){ 
  for(let i = 0; i < iconCross.length; i++){
    iconCross[i].addEventListener("mousedown", closeCrossButtonPressDown, false) || iconCross[i].addEventListener("touchstart", closeCrossButtonPressDown, false);
    let mouseUporOut = iconCross[i].addEventListener("mouseup", closeCrossButtonPressUp, false) 
    || iconCross[i].addEventListener("mouseleave", closeCrossButtonPressUp, false) 
    || iconCross[i].addEventListener("touchmove", closeCrossButtonPressUp, false)
    || iconCross[i].addEventListener("touchend", closeCrossButtonPressUp, false);
  }
}
});
function closeCrossButtonPressDown(e){
  e.target.closest('.cross').querySelector('.icon').classList.add('active');   
}
function closeCrossButtonPressUp(e){
  e.target.closest('.cross').querySelector('.icon').classList.remove('active');
}
// close icons animation end

// media query
if(mobileMenu){
	function controllVpSize(x) {
		if (x.matches && !menuColumn.classList.contains('column_hidden')) { // If media query matches
			mobileMenu.classList.add('hidden');
			menuColumn.classList.add('column_hidden');
      if(siteContentWrap){
        siteContentWrap.classList.remove('offset');
      }  
	  } 
    if (!x.matches){
      menuScrollBar.update();
    }
	}
  function orientation(){
    menuScrollBar.update();
  }
  let x = window.matchMedia("(min-width: 992px)");
	let pos = window.matchMedia("(orientation: portrait)");
	controllVpSize(x); // Call listener function at run time
	x.addListener(controllVpSize);
  orientation(pos);
  pos.addListener(orientation);
}
// media query end  

// mobile menu end
// video page start
if(document.body.classList.contains('videos_single_page')){
    let modal = new tingle.modal({
      footer: false,
      stickyFooter: false,
      closeMethods: ['overlay', 'button', 'escape'],
      closeLabel: "Close",
      onClose: function() {
          modal.setContent(defaultModalContent);
      }
  }),
  defaultModalContent = `<h3 class="modal_heading"></h3>
    <div class="video_content">
      <div class="image_loader_cnt image_loader_cnt--iframe">
        <div class="simple-spinner simple-spinner--xl"></div>
      </div>
    </div>`,
  videoButonTrig = document.querySelectorAll('.video_page__item>a'),
  fetchURL = document.querySelector('.video_page').getAttribute('data-url');
  for(let i = 0; i < videoButonTrig.length; i++){
    videoButonTrig[i].addEventListener('click', requestVideoIframe);
    videoButonTrig[i].addEventListener('click', function(){
      modal.open();
    });
  }
  modal.setContent(defaultModalContent);
  function requestVideoIframe(e){
    e.preventDefault();
    let videoData = new FormData(),
    id = e.target.closest('.video_id').getAttribute('data-id'),
    options = {
      method: 'POST',
      body: videoData
    };
    videoData.append('action', 'cas_request_video_iframe');
    videoData.append('id', id);
    let videoReq = new Request(fetchURL, options);
    
    fetch(videoReq)
    .then((response)=>{
      if(response.ok){
        return response.text();
      }else{
        throw new Error('Bad HTTP');  
      }
    })
    .then(iframe =>{
      modal.setContent(iframe);
    })
    .then(()=>{
      let video = document.querySelector('.video_content>iframe');
      if(video){
      let str = video.getAttribute("src");
        if(/youtube\.com+/.test(str) === true){
          str+="&autoplay=1";
          video.setAttribute("src", str);
        }else if(/vimeo\.com+/.test(str) === true){
          str+="&autoplay=1";
          video.setAttribute("src", str);
        }else if(/dailymotion\.com+/.test(str) === true){
          str+="&autoplay=1";
          video.setAttribute("src", str);
        }else{
          return
        }
      }  
    })
    .catch((err) =>{
      if(err.message){
        modal.setContent('Failed to load, plz refresh the page(');
        console.log(err.message);
      }else{
        console.log('Failed to load, plz refresh the page('); 
      }
    });

  }
}
// video page end