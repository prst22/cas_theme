postReveal.requestPosts = function(event){
  if(event.type !== 'popstate'){
    event.preventDefault();
    
  } else {
    alert("popstate");
  }
  
  let targetPaginationPage = (!isNaN(Number(event.target.textContent))) ? Number(event.target.textContent) : 1,
  search,
  lg = window.matchMedia("(min-width: 992px)"),
  offsetAmount = (document.body.classList.contains('home') && lg.matches) ? -96 : -48,
  dataNumber = targetPaginationPage,
  pagCnt = document.getElementsByClassName('pagination_cnt__inner')[0],
  postsCnt = document.querySelector('.posts_cnt') || document.querySelector('.results'),
  bottomNotification = document.querySelector('.rocbot'),
  cat = document.getElementsByClassName('check_sort'),
  formData = new FormData();
  const ajaxUrl = sortButton ? sortButton.getAttribute('data-url') : pagCnt.getAttribute('data-url');
  if(searchPagination !== null){
    search = document.querySelector('.pagination_cnt--search>.pagination_cnt__inner').getAttribute('data-search');
    formData.append('search', search);
  }else{
    search = 0;
    formData.append('search', search);
    let checkedCategory = Array.from(cat).filter((checkedCat, index) =>{
    if(checkedCat.checked == true){
      return checkedCat;
    }
    });
    if(checkedCategory.length != 0){
      checkedCategory.forEach((catToSend, index)=>{
        formData.append(catToSend.getAttribute('id'), catToSend.getAttribute('value'));
      });
    }
  }

  let postloaderContent = document.querySelector('.posts_cnt__loader_cnt'),
  postLoader = document.querySelector('.posts_cnt__loader_cnt');
	formData.append('number', dataNumber);
	formData.append('action', 'request_posts');
  let currentPageElement = document.querySelector('.pagination_cnt>.pagination_cnt__inner>.current'), 
  options = {
  	method: 'POST',
  	body: formData
  },
  req = new Request(ajaxUrl, options);
  if(currentPageElement) currentPage = currentPageElement.textContent;
  postsCnt.removeChild(document.querySelector('.post_item'));
  postLoader.style.display = "block";
  postLoader.appendChild(postReveal.appendLoader());
  let promiseWithNewChunkOfPosts = (function(){
    let data = fetch(req);
      return data;
    })(); 
  jump_module(scrollTarget, {
    duration: 1000,
    offset: offsetAmount,
    callback: () => {
      promiseWithNewChunkOfPosts
      .then((response)=>{
        if(response.ok){
          return response.text();
        }else{
          throw new Error('Bad HTTP');  
        }
      })
      .then((text)=>{
        if(text !== ''){
          let newPageWithPosts = document.createElement('DIV');
          newPageWithPosts.innerHTML = text;
          let newPosts = newPageWithPosts.firstChild;
          //clear previous page for new posts 
          postLoader.style.display = "none";
          postsCnt.appendChild(newPosts);
          if(pagCnt){
            pagCnt.setAttribute('data-number', Number(dataNumber));
          }
          if(currentPageElement){
            document.body.classList.remove("paged-" + currentPage);
            document.body.classList.add("paged", "paged-" + targetPaginationPage);
          }
          updateUrl(newPosts);
          if(pagCnt){
            postReveal.updatePagination();
          }
          return postsCnt
        }
      })
      .then((newPostsAdded)=>{
        let elementToAnimate = newPostsAdded.querySelector('.post_item:not(.added)');
        if(postsCnt.parentNode.querySelector('.loading')) postLoader.removeChild(postsCnt.parentNode.querySelector('.loading'));
        return {
          newPosts: newPostsAdded,
          element: elementToAnimate
        };
      }) 
      .then((obj)=>{
        if(obj.newPosts){
            let promise = new Promise((resolve, reject) => {
              animationObj.animationFunction({
                duration: 300,
                timing: ease,
                startPoint: 0,
                draw: function(progress) {
                  obj.element.style.transform = "translate3d(" + ((1 - progress) * 100) + "px, 0, 0)";            
                  obj.element.style.opacity = progress;            
                }
              });
              let btn = document.querySelectorAll('.pagination_cnt__inner>a.page-numbers');
              if(btn && btn.length >= 1){
                for(let i = 0; i < btn.length; i++){
                  btn[i].addEventListener('click', postReveal.requestPosts);
                }
              }
              resolve();
            });
            promise.then(
                result => {
                  bLazy.load(document.querySelectorAll('.thumbnail_img'), true);
                },
                error => {
                  console.log("Rejected: " + error);
                }
            );
        }else{
            postLoader.appendChild(postReveal.failed());
            setTimeout(()=>{
                postLoader.querySelector('.rocbot').classList.add('reveal'); 
            }, 100);
        }
      })
      .catch((err) =>{
        if(err.message){
          console.log(err.message);
          if(postsCnt.querySelector('.loading')) postLoader.removeChild(postsCnt.querySelector('.loading'));
          postLoader.appendChild(postReveal.failed());
          setTimeout(()=>{
            postLoader.querySelector('.rocbot').classList.add('reveal'); 
          }, 100);        
        }else{
          console.log(err);
        }
      });   
    }
  });
}
