<?php
/* Template Name: Contact Page */
?>
<?php get_header();?>
	 <main class="container main">
	    <div class="row">
	        <div class="col-12">
	            <div class="single_page single_page_to_animate">
	                <?php while ( have_posts() ) : the_post(); ?>
	                    <h1 class="mt-5 mb-3 text-center"><?php the_title(); ?></h1>
	                    <div class="d-flex justify-content-center align-items-center">
	                    <?php echo do_shortcode('[contact_form]'); ?>
	                    </div>
	                <?php endwhile; ?>
	            </div>
	        </div>
	    </div>
	</main>
<?php get_footer();?>