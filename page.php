<?php get_header();?>
    <main class="container main">
        <div class="row">
            <div class="col-12">
                <div class="single_page single_page_to_animate position-relative box_shadow box_shadow--sin_pos">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div class="single_page__inner main_post">
                        <div class="single_page__post_heading">
                            <h1 class="mb-3 text-center heading_title"><?php the_title(); ?></h1>
                        </div>
                        <div class="content_here">
                            <?php  the_content();  ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </main>
<?php get_footer();?>