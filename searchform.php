<form method="get" class="searchform form-inline <?php echo (is_search() ? '' : 'text-center'); ?>  search_form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" class="search_field" name="s" placeholder="<?php esc_attr_e( 'search' ); ?>" title="search" size="15"/>
	<button class="btn btn--search" type="submit" name="submit">
		<svg class="icon icon-search"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-search"></use></svg>
	</button>
</form>