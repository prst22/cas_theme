<?php
/* Template Name: Videos Page */
?>
<?php
	get_header();
?>
<main class="container main">
	<section class="row video_page single_page_to_animate" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
		<?php 
			$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
			$video_query = new WP_Query(
	        	array(
	        		'post_type' => 'post',
					'posts_per_page' => 21,
	        		'post_status' => 'publish',
	        		'paged' => $paged,
	        		'tax_query' => array( 
			        		array(
					            'taxonomy' => 'post_format',
					            'field' => 'slug',
					            'terms' => 'post-format-video',
				            )
		        		),
					'update_post_meta_cache' => false, 
					'update_post_term_cache' => false 
	        	)
	        );
	        
	        if($video_query->have_posts()):
	        	while($video_query->have_posts()): $video_query->the_post();?> 
		        	<div class="col-12 col-md-6 col-xl-4 video_page__item">
		        		<a href="#" class="video_id" data-id="<?php echo get_the_ID(); ?>">
		        			<figure>
		        				<h4 class="video_page_item_title position-absolute">
		        					<span><?php the_title(); ?></span>
		        				</h4>
		        				<div class="video_play_btn video_play_btn--video_page">
									<span>PLAY</span>
									<i>
										<svg class="icon play-circle"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play-circle"></use></svg>
									</i>
								</div>
				        		<?php
					        		if(has_post_thumbnail()):
						        		the_post_thumbnail('medium-thumbnail', 
										$attr = array(
											'class' => "video_thumbnail thumbnail_img_v_p",
											'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'medium-thumbnail'),
											'alt' => get_the_title()
										));
                                    else:?>
                                    <img src="<?php echo get_template_directory_uri() .'/inc/images/video_placeholder.png'; ?>" class="video_thumbnail thumbnail_img_v_p" data-src="<?php echo get_template_directory_uri() .'/inc/images/video_placeholder.png'; ?>" alt="<?php the_title(); ?>">
									<?php endif;	 
								?>
								<div class="image_loader_cnt image_loader_cnt--left">
									<div class="simple-spinner">
									</div>
								</div> 
							</figure>
						</a>
		        	</div>
				<?php endwhile;?>
				
	        <?php else: ?>
	        	<div class="col-12">
		        	<h1 class="text-center mt-5 mb-3 no_results_found"><?php echo esc_html__('No videos', 'cas_theme'); ?></h1>
	        	</div>
	        	<?php wp_reset_postdata(); ?>
	        <?php endif; ?>

	</section>
	<?php 
		if($video_query->max_num_pages > 1):
			echo '<div class="row">';
			echo '<div class="col-12">';
			echo '<div class="pagination_cnt">';
			echo '<div class="pagination_cnt__inner">';
				echo paginate_links(array(
					'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
					'format'       => '?paged=%#%',
					'prev_next'    => false,
					'type'         => 'plain',
					'total'        => $video_query->max_num_pages,
					'current'      => $paged,
					'end_size'     => 2,
					'mid_size'     => 8
				));
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
	    endif;
	 ?>
</main>
<?php		
	get_footer();
?>