<?php
/* Template Name: Live Streams Page */
?>
<?php
	get_header();
?>
<main class="container main">
	<?php
	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$date = new DateTime();
    $current_time = $date->getTimestamp();
    $future_time = $current_time + 50;
	$streamers_arr = get_meta_values('_streams_id_value_key', 'streams' );
	$games_arr = array('poker'=> '488190', 'slots'=> '498566', 'roulette' => '490344', 'blackjack'=> '104');
	$url = 'https://api.twitch.tv/helix/streams/?';
	foreach($streamers_arr as $user_id){
		if(isset($user_id) && !empty($user_id)){
	        $url .= 'user_id='. $user_id . '&';
        }
	}
	$url = rtrim($url, '&');
    $opts = [
	    "http" => [
	        "method" => "GET",
	        "header" => "Client-ID: vj6rb2kg6ft1vw2w0z42y6ysehddpv\r\n"
	    ]
	];
	
	class Stream{
		function __construct($url, array $opts, array $games){
            $this->url = $url;
            $this->options = $opts;
            $this->games = $games;
		}
	    public function parse_data_to_array(){
	    	$context = stream_context_create($this->options);
			$file = file_get_contents($this->url, false, $context);
			$response = json_decode($file);
            $streamers_arr = array();
            //var_dump($response);
		    if(isset($response->data)){
			    if(count($response->data) > 0) {
				    foreach ($response->data as $stream_obj) {
				    	if( $stream_obj->game_id === $this->games['poker'] || $stream_obj->game_id === $this->games['slots'] || $stream_obj->game_id === $this->games['roulette'] || $stream_obj->game_id === $this->games['blackjack'] ){
				    		array_push($streamers_arr, $stream_obj->user_id);
					    	$streamer_name = ucfirst(strtolower($stream_obj->user_name));
					    	$raw_title = preg_replace('/[^\p{L}\s\d.,:\/]/u', '', mb_convert_encoding($stream_obj->title, "UTF-8")); 
					    	$stream_title = strlen($raw_title) > 38 ? mb_substr($raw_title, 0, 38, "UTF-8") . "..." : $raw_title;
					    	$stream_title_formated = mb_convert_case($stream_title, MB_CASE_TITLE, "UTF-8");
					    	$stream_language = $stream_obj->language;
					    	$stream_img = $stream_obj->thumbnail_url;
					    	$stream_img = str_replace("{width}", "400", $stream_img);
					    	$stream_img = str_replace("{height}", "225", $stream_img);
					    	$game_id = $stream_obj->game_id;
				            $viewer_count = $stream_obj->viewer_count;
		                    $streamers_arr[$stream_obj->user_id] = array('name' => $streamer_name, 'title' => $stream_title_formated, 'lan' => $stream_language, 'img_src' => $stream_img, 'game' => $game_id, 'viewers' => $viewer_count);
				    	}
				    }
				    return $streamers_arr;
				}
			}
	    }
	}; 
	// user id 139736417
	// user 'https://api.twitch.tv/helix/users?login=ryanfeepoker'
    // 'https://api.twitch.tv/helix/streams?game_id=488190'
    // 'https://api.twitch.tv/helix/streams?game_name=Blackjack'
    // 'https://api.twitch.tv/helix/streams?user_id=139736417'
    // [^\p{L}\s]
	// Open the file using the HTTP headers set above start
	if(!isset($_SESSION['timestamp']) && empty($_SESSION['timestamp'])){
		$streamData = new Stream($url, $opts, $games_arr);
		$streamers_arr_for_query = $streamData->parse_data_to_array();
		$_SESSION['savad_streams'] = $streamers_arr_for_query;
		$saved_streamers_arr_for_query = $_SESSION['savad_streams'];
		$_SESSION['timestamp'] = $future_time;
	} else {
		if($current_time >= $_SESSION['timestamp']){
			$streamData = new Stream($url, $opts, $games_arr);
			$streamers_arr_for_query = $streamData->parse_data_to_array();
			$_SESSION['timestamp'] = $future_time;
			if(!empty($streamers_arr_for_query)){
				$_SESSION['savad_streams'] = $streamers_arr_for_query;
			    $saved_streamers_arr_for_query = $_SESSION['savad_streams'];
			    $_SESSION['timestamp'] = $future_time;
			} else {
                $_SESSION['savad_streams'] = array();
                $saved_streamers_arr_for_query = $_SESSION['savad_streams'];
                $_SESSION['timestamp'] = $future_time;
			}
		} else {
			$saved_streamers_arr_for_query = $_SESSION['savad_streams'];
		}
	} 
    // Open the file using the HTTP headers set above end
	     ?>
    <?php if( !empty($streamers_arr_for_query) || (!empty($saved_streamers_arr_for_query)) ): ?>
	    <section class="pt-5 d-flex flex-wrap justify-content-between align-items-center controles_cnt">
		    <h2 class="controles_cnt__title">Live Streams</h2>
		    <div class="px-1 px-sm-0 py-4 py-sm-2 controles_cnt__buttons">
		    	<button class="button is_checked" data-filter="*">ALL</button>
		    	<button class="button" data-filter=".casino">CASINO</button>
		    	<button class="button" data-filter=".poker">POKER</button>
		    	<button class="button" data-sort-value="viewers">VIEWERS</button>
		    	<button class="button is_checked" data-sort-value="original-order">NAME</button>
		    </div>
		</section> 
    <?php endif; ?> 

    <?php
		if(!empty($streamers_arr_for_query) || !empty($saved_streamers_arr_for_query)): ?>
		<section class="stream_page live_streams_cnt single_page_to_animate">
			<div class="grid-sizer"></div>
			
			<?php
			    // create currently active streams array for wp query start
			    $meta_array = array();
				$meta_array['relation'] = 'OR'; 
                
                if(!empty($streamers_arr_for_query)){
                	foreach($streamers_arr_for_query as $id){  

					    $meta_array[] = array(
					        'key'       => '_streams_id_value_key',  
					        'value'     => (int)$id,  
					        'compare'   => '=',  
					        'type'      => 'NUMERIC',
					    );
					}
                } elseif (!empty($saved_streamers_arr_for_query)) {
                	foreach($saved_streamers_arr_for_query as $key => $id){  

					    $meta_array[] = array(
					        'key'       => '_streams_id_value_key',  
					        'value'     => (int)$key,  
					        'compare'   => '=',  
					        'type'      => 'NUMERIC',
					    );
					}
                }
				
				// create currently active streams array for wp query end
				$args = array(
				    'post_type'      => 'streams',
				    'posts_per_page' => 15,
				    'paged'          => $paged,
				    'orderby'        => 'title',
				    'order'          => 'ASC',
				    'meta_query'     => $meta_array
				);
					
				
	            $stream_loop = new WP_Query( $args );
				if ( $stream_loop->have_posts()) :

					while ( $stream_loop->have_posts() ) :
						$stream_loop->the_post();
						$steam_id = get_post_meta( get_the_ID(), '_streams_id_value_key', true );
						if(!empty($streamers_arr_for_query)){
							$img = $streamers_arr_for_query[$steam_id]['img_src'];
							$stream_tit = $streamers_arr_for_query[$steam_id]['title'];
							$stream_lan = $streamers_arr_for_query[$steam_id]['lan'];
							$stream_nam = $streamers_arr_for_query[$steam_id]['name'];
							$game_type = $streamers_arr_for_query[$steam_id]['game'];
							$view = $streamers_arr_for_query[$steam_id]['viewers'];
						} elseif (!empty($saved_streamers_arr_for_query)){
							$img = $saved_streamers_arr_for_query[$steam_id]['img_src'];
							$stream_tit = $saved_streamers_arr_for_query[$steam_id]['title'];
							$stream_lan = $saved_streamers_arr_for_query[$steam_id]['lan'];
							$stream_nam = $saved_streamers_arr_for_query[$steam_id]['name'];
							$game_type = $saved_streamers_arr_for_query[$steam_id]['game'];
							$view = $saved_streamers_arr_for_query[$steam_id]['viewers'];
						}
						?>
						<div class="stream_page__item <?php echo (($game_type === '488190') ? 'poker' : 'casino' ); ?>">
							<a href="<?php the_permalink(); ?>">
								<figure>
									<h4 class="stream_page_item_title position-absolute"><span><?php echo $stream_tit; ?></span></h4>
			                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo $img; ?>" class="thumbnail_img_s_p">
									<div class="image_loader_cnt image_loader_cnt--center">
										<div class="simple-spinner">
										</div>
									</div> 
								</figure>
							</a>
							<div class="d-inline-flex live">
								<div class="live__inner">
									<span class="live_tag">LIVE</span>
								</div>
								<div class="live__inner">
									<span class="live_language"><?php echo $stream_lan; ?></span>
								</div>
							</div>
							<div class="name">
								<span class="name__inner"><?php echo $stream_nam; ?></span>
							</div>
							<div class="viewers">
								<span class="viewers__inner">
									<span class="pr-1 viewer_count_icon">
										<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
									</span>
									<span class="number">
										<?php echo $view; ?>
									</span>
							    </span>
							</div>
			        	</div>
					<?php endwhile; // end while
					wp_reset_postdata();
				    else:?>
						<h1 class="text-center mt-5 mb-3 no_results_found">No live streams right now</h1>
				<?php endif; ?>
	    </section>

	    <?php 
			if($stream_loop->max_num_pages > 1):
				echo '<div class="row">';
				echo '<div class="col-12">';
				echo '<div class="pagination_cnt">';
				echo '<div class="pagination_cnt__inner">';
					echo paginate_links(array(
						'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
						'format'       => '?paged=%#%',
						'prev_next'    => false,
						'type'         => 'plain',
						'total'        => $stream_loop->max_num_pages,
						'current'      => $paged,
						'end_size'     => 2,
						'mid_size'     => 8
					));
				echo '</div>';
				echo '</div>';
				echo '</div>';
				echo '</div>';
		    endif; ?>

	    <?php else: ?>
	    	<section class="row video_page single_page_to_animate">
	    		<div class="col-12">
	    			<h1 class="text-center mt-5 mb-3 no_results_found">No live streams right now</h1>
	    		</div>
		    </section>
	    <?php endif; ?>

    
</main>
<?php		
	get_footer();
?>