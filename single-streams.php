<?php get_header(); ?>

    <main class="container main single_post_main">
        
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            
            get_template_part( 'template-parts/content-streams');

            ?>
            <div class="row mb-5">
                <div class="col-12 pt-4 pt-sm-0">
                    
                    <div class="back_to_live_st">
                        <?php $live_streams = get_page_by_title('live streams'); ?>
                        <a href="<?php the_permalink($live_streams); ?>" class="btn btn--back_to_live_strems">
                            <span>
                                <i class="back">
                                    <svg class="icon icon-back"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-back"></use></svg>
                                </i> 
                            Back to <?php echo strtolower($live_streams->post_title); ?>
                            </span>
                        </a>
                    </div>
                    
                </div>
            </div>
        <?php 
	        // End the loop.
	        endwhile;
        ?>
    </main><!-- .site-main -->
<?php get_footer(); ?>