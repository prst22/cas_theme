<?php
    require get_template_directory() . '/inc/ajax.php';
    require get_template_directory() . '/inc/customizer.php';
    require get_template_directory() . '/inc/function-admin.php';
    require get_template_directory() . '/inc/custom-post-type.php';
    require get_template_directory() . '/inc/shortcodes.php';
    require get_template_directory() . '/inc/widget.php';

    //add theme support start

    function theme_setup() {
    	load_theme_textdomain( 'cas_theme' );

    	// Adding menus to wp controll pannel
		register_nav_menus(array(
			'main_menu' =>__('Main menu'),
			'footer_menu' =>__('Footer Menu')
		));

	    //post thum pictures
	    add_theme_support('post-thumbnails');

	    add_image_size('small-thumbnail', 180, 101, array('center','center'));
	    add_image_size('medium-thumbnail', 787, 295, array('center','center'));  // 500, 281,
	    add_image_size('large-thumbnail', 1200, 450, array('center','center'));
	   
	    add_theme_support( 'post-formats', array( 'video', 'link', 'quote' ));

    	//CUSTOM WORDPRESS EDITOR STYLE
		add_editor_style( 'css/custom-editor-styles.min.css' );
	    //CUSTOM WORDPRESS EDITOR STYLE 
	    add_theme_support( 'customize-selective-refresh-widgets' );

    }
    add_action( 'after_setup_theme', 'theme_setup' );

	//add theme support end


    //adding css styles start
	function add_styles_js(){
		wp_enqueue_style('style', get_theme_file_uri( '/css/main.min.css'), array(), '1.3');
		wp_enqueue_style('style-sm', get_theme_file_uri( '/css/main_sm.min.css'), array(), '1.3', 'screen and (min-width: 576px)');
		wp_enqueue_style('style-md', get_theme_file_uri( '/css/main_md.min.css'), array(), '1.3', 'screen and (min-width: 768px)');
		wp_enqueue_style('style-lg', get_theme_file_uri( '/css/main_lg.min.css'), array(), '1.3', 'screen and (min-width: 992px)');
		wp_enqueue_style('style-xl', get_theme_file_uri( '/css/main_xl.min.css'), array(), '1.3', 'screen and (min-width: 1200px)');
		wp_enqueue_style('style-xxl', get_theme_file_uri( '/css/main_xxl.min.css'), array(), '1.3', 'screen and (min-width: 1650px)');
		wp_enqueue_style('style-xxxl', get_theme_file_uri( '/css/main_xxxl.min.css'), array(), '1.3', 'screen and (min-width: 1850px)');
		if(is_page_template('page-live-streams.php')){
			wp_enqueue_script('isotope', get_template_directory_uri() . '/js/is.min.js', array(), 1.0, true);
		}
		wp_enqueue_script('lazy_load', get_template_directory_uri() . '/js/blazy.min.js', array(), 1.0, true);
		if(is_front_page()){
			wp_enqueue_script('vivus', get_template_directory_uri() . '/js/vivus.min.js', array(), 1.0, true);
		}
		if(is_page_template('page-videos.php')){
			wp_enqueue_style('tingle', get_theme_file_uri( '/css/tingle.min.css'));
			wp_enqueue_script('tingle_js', get_template_directory_uri() . '/js/tingle.min.js', array(), 1.0, true);
		}
		wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', array(), 1.3, true);
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action('wp_enqueue_scripts', 'add_styles_js');

	//adding css styles end

    function custom_post_type() {
	    // Set UI labels for Custom Post Type
	    $labels = array(
	        'name'                => _x( 'Streams', 'Post Type General Name', 'cas_theme' ),
	        'singular_name'       => _x( 'Stream', 'Post Type Singular Name', 'cas_theme' ),
	        'menu_name'           => __( 'Streams', 'cas_theme' ),
	        'parent_item_colon'   => __( 'Parent Streams', 'cas_theme' ),
	        'all_items'           => __( 'All Streams', 'cas_theme' ),
	        'view_item'           => __( 'View Streams', 'cas_theme' ),
	        'add_new_item'        => __( 'Add New Streams', 'cas_theme' ),
	        'add_new'             => __( 'Add New', 'cas_theme' ),
	        'edit_item'           => __( 'Edit Stream', 'cas_theme' ),
	        'update_item'         => __( 'Update Streams', 'cas_theme' ),
	        'search_items'        => __( 'Search Streams', 'cas_theme' ),
	        'not_found'           => __( 'Not Found', 'cas_theme' ),
	        'not_found_in_trash'  => __( 'Not found in Trash', 'cas_theme' ),
	    );
	     
	    // Set other options for Custom Post Type	     
	    $args = array(
	        'label'               => __( 'streams', 'cas_theme' ),
	        'description'         => __( 'Streams', 'cas_theme' ),
	        'labels'              => $labels,
	        // Features this CPT supports in Post Editor
	        'supports'            => array( 'title', 'revisions', 'custom-fields' ),
	        /* A hierarchical CPT is like Pages and can have
	        * Parent and child items. A non-hierarchical CPT
	        * is like Posts.
	        */ 
	        'hierarchical'        => false,
	        'public'              => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_nav_menus'   => false,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 6,
	        'can_export'          => true,  
	        'exclude_from_search' => false,
	        'publicly_queryable'  => true,
	        'capability_type'     => 'page',
	         // You can associate this CPT with a taxonomy or custom taxonomy. 
	        'taxonomies'          => array( 'type' ),
	        'query_var'           => true, 
	        'rewrite' => array('slug' => 'streams/%type%'),
	        'menu_icon'			=> 'dashicons-welcome-view-site',
	        // rewrite link adress
	    );
	     
	    // Registering your Custom Post Type
	    register_post_type( 'streams', $args );
	 
	}
	 
	/* Hook into the 'init' action so that the function
	* Containing our post type registration is not 
	* unnecessarily executed. 
	*/
	add_action( 'init', 'custom_post_type', 0 );

	function create_private_live_streams_tax() {
	    $labels = array(
			'name'              => _x( 'Types', 'taxonomy general name', 'cas_theme' ),
			'singular_name'     => _x( 'Type', 'taxonomy singular name', 'cas_theme' ),
			'search_items'      => __( 'Search Types', 'cas_theme' ),
			'all_items'         => __( 'All Types', 'cas_theme' ),
			'parent_item'       => __( 'Parent Type', 'cas_theme' ),
			'parent_item_colon' => __( 'Parent Type:', 'cas_theme' ),
			'edit_item'         => __( 'Edit Type', 'cas_theme' ),
			'update_item'       => __( 'Update Type', 'cas_theme' ),
			'add_new_item'      => __( 'Add New Type', 'cas_theme' ),
			'new_item_name'     => __( 'New Type Name', 'cas_theme' ),
			'menu_name'         => __( 'Type', 'cas_theme' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => false,
			'rewrite'           => array( 'slug' => 'type'),
		);

		register_taxonomy( 'type', 'streams', $args );
	}

	add_action( 'init', 'create_private_live_streams_tax' );

    // add custom field to streams cpt start

    function cas_theme_streams_id_add_meta_box() {
		add_meta_box( 'streamer_id', 'Streamer id', 'cas_theme_streams_id_callback', 'streams', 'normal', 'high' );
	}
	function cas_theme_streams_id_callback( $post ) {
		wp_nonce_field( 'cas_streams_id_meta_box_data', 'cas_streams_id_meta_box_data_nonce' );
		
		$value = get_post_meta( $post->ID, '_streams_id_value_key', true );
		
		echo '<label for="cas_stream_id_field">Streamer user id: </label>';
		echo '<input type="text" id="cas_stream_id_field" name="cas_stream_id_field" value="' . esc_attr( $value ) . '" size="25" />';
	}
	function cas_streams_id_meta_box_data( $post_id ) {
		
		if( ! isset( $_POST['cas_streams_id_meta_box_data_nonce'] ) ){
			return;
		}
		
		if( ! wp_verify_nonce( $_POST['cas_streams_id_meta_box_data_nonce'], 'cas_streams_id_meta_box_data') ) {
			return;
		}
		
		if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
			return;
		}
		
		if( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
		
		if( ! isset( $_POST['cas_stream_id_field'] ) ) {
			return;
		}
		
		$my_data = sanitize_text_field( $_POST['cas_stream_id_field'] );
		
		update_post_meta( $post_id, '_streams_id_value_key', $my_data );
		
	}

	add_action( 'add_meta_boxes', 'cas_theme_streams_id_add_meta_box' );
	add_action( 'save_post', 'cas_streams_id_meta_box_data' );
    // add custom field to streams cpt end

    function wpa_course_post_link( $post_link, $id = 0 ){
	    $post = get_post($id);  
	    if ( is_object( $post ) ){
	        $terms = wp_get_object_terms( $post->ID, 'type' );
	        if( $terms ){
	            return str_replace( '%type%' , $terms[0]->slug , $post_link );
	        }
	    }
	    return $post_link;  
	}
	add_filter( 'post_type_link', 'wpa_course_post_link', 1, 3 );
    
    // change default url for custom post start(like uncategorized for regular posts)

    function default_taxonomy_term( $post_id, $post ) {
	    if ( 'publish' === $post->post_status ) {
	        $defaults = array(
	            'type' => array( 'Casino' ),
	            );
	        $taxonomies = get_object_taxonomies( $post->post_type );
	        foreach ( (array) $taxonomies as $taxonomy ) {
	            $terms = wp_get_post_terms( $post_id, $taxonomy );
	            if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
	                wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
	            }
	        }
	    }
	}
	add_action( 'save_post', 'default_taxonomy_term', 100, 2 );

    // change default url for custom post end

	//Controll post length on the main page

	function set_excerpt_length(){
		if(get_post_format() == 'video'){
			return 25;
		}else{
			return has_post_thumbnail() ? 25 : 50;
		}
	}
	add_filter('excerpt_length', 'set_excerpt_length');

	function cas_theme_custom_excerpt() {
	    return '...';
	}
	add_filter( 'excerpt_more', 'cas_theme_custom_excerpt' );

	//widgets locations start
	function cas_theme_init_widgets($id){
		register_sidebar(array(
		    'name'         => 'Side Panel',
		    'id'           => 'side_panel',
		    'before_widget'=> '<div class="side_panel">',
		    'after_widget' => '</div>',
		    'before_title' => '',
		    'after_title'  => ''
		));
		register_sidebar(array(
		    'name'         => 'Footer widget',
		    'id'           => 'footer_widget',
		    'before_widget'=> '<div class="footer_content__footer_widget">',
		    'after_widget' => '</div>'
		));
	}
	add_action('widgets_init', 'cas_theme_init_widgets');
	//widgets locations end

	//search input start
    function wpdocs_after_setup_theme() {
	    add_theme_support( 'html5', array( 'search-form' ) );
	}
	add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );
    //search input end

	// Add responsive container to embeds
 
	function embed_html( $html ) {
	   return '<div class="video_content">' . $html . '</div>';
	}
	add_filter( 'embed_oembed_html', 'embed_html', 10, 3 );

	// removing url field in comments section start
	function disable_url_in_comments_field($fields){
		if(isset($fields['url']))
		unset($fields['url']);
		return $fields;
	}
    add_filter('comment_form_default_fields', 'disable_url_in_comments_field');
    
	// First, create a function that includes the path to your favicon
	function add_favicon() {
	  	$favicon_url = get_template_directory_uri() . '/admin-favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" type="image/x-icon" sizes="32x32" />';
	}
	  
	// Now, just make sure that function runs when you're on the login page and admin pages  
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');
    // count amount of views of a post start
    function save_post_views( $postID ) {
		
		$metaKey = 'cas_theme_post_views';
		$views = get_post_meta( $postID, $metaKey, true );
		
		$count = ( empty( $views ) ? 0 : $views );
		$count++;
		
		update_post_meta( $postID, $metaKey, $count );
		
	}
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // count amount of views of a post end
    
    //exclude pages from search

    function my_post_queries( $query ) {
		// do not alter the query on wp-admin pages and only alter it if it's the main query

		if (!is_admin() && $query->is_main_query()){

			if (is_search()) {
				$query->set('post_type', array('post', 'streams'));
			}
		}
	}
	add_action( 'pre_get_posts', 'my_post_queries' );

    // get url for archive page

	// function current_uri() {
	// 	$http = ( isset( $_SERVER["HTTPS"] ) ? 'https://' : 'http://' );
	// 	$referer = $http . $_SERVER["HTTP_HOST"];
	// 	$archive_url = $referer . $_SERVER["REQUEST_URI"];
		
	// 	return $archive_url;
	// }
	// adding classes for video page

	add_filter( 'body_class', 'custom_class' );
	function custom_class( $classes ) {
	    if ( is_page_template( 'page-videos.php' ) ) {
	        $classes[] = 'videos_single_page';
	    }
	    if ( is_page_template( 'page-live-streams.php' ) ) {
	        $classes[] = 'live_streams';
	    }
	    return $classes;
	}
    function register_my_session(){
	  if( !session_id() ){
	    session_start();
	  }
	}

	add_action('init', 'register_my_session');

	function get_meta_values( $key = '', $type = 'post', $status = 'publish' ) {
	    global $wpdb;
	    if( empty( $key ) )
	        return;
	    $r = $wpdb->get_col( $wpdb->prepare( "
	        SELECT pm.meta_value FROM {$wpdb->postmeta} pm
	        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
	        WHERE pm.meta_key = '%s' 
	        AND p.post_status = '%s' 
	        AND p.post_type = '%s'
	    ", $key, $status, $type ) );
	    return $r;
	}
	// wp-post rating removing alt text on images start
	add_filter( 'wp_postratings_ratings_image_alt', 'wp_postratings_ratings_image_alt' );  
	function wp_postratings_ratings_image_alt( $alt_title_text ) {  
		return '';  
	} 
	// wp-post rating removing alt text on images end