<?php get_header(); ?>

	<main class="container main archive_page">
        <div class="row">
            <div class="col-12 mh">

			<?php if ( have_posts() ) : ?>

				<header class="text-center mt-5">
					<?php
						the_archive_title( '<h1 class="archive_page__title">', '</h1>' );
					?>
				</header><!-- .page-header -->
                
				<?php
				// Start the Loop.
				echo '<div class="post_item">';
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/index', 'posts' ); 

					// End the loop.
				endwhile;
                echo '</div>';
					// Previous/next page navigation.
					echo '<div class="pagination_cnt">';
						echo '<div class="pagination_cnt__inner">';
								echo paginate_links(array(
									'prev_next'          => false,
									'show_all'           => true,
									'type'               => 'plain',
									'end_size'           => 2,
									'mid_size'           => 8
									));
						echo '</div>';
					echo '</div>';
                
				// If no content, include the "No posts found" template.
			else :
				echo '<h1 class="text-center mt-5 mb-3 no_results_found">' . esc_html__( "No results", "cas_theme" ) . '</h1>';
			endif;
			?>

			</div>
		</div>
	</main>

<?php
get_footer();