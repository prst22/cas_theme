<?php
/*
Plugin Name: Selected Posts in Widget
Plugin URI: https://timeandupdate.com/selected_posts_in_widget/
Description: Add Selected Posts in your Widget Area.
Version: 1.0.1
Author: Time and Update
Author URI: http://www.potatosites.com/
License: TCIY
*/

// The widget class
class Select_Best_Cas_in_Widget extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'select_Best_Cas_in_widget',
			__( 'Select Best Casinos', 'cas_theme' ),
			array(
				'customize_selective_refresh' => true,
			)
		);
	}
	public function form( $instance ) {
		$defaults = array(
			'title'      => 'Best online casinos',
			'select'     => '',
			'select2'    => '',
			'select3'    => '',
			'select4'    => '',
			'select5'    => '',
			'select6'    => '',
			'select7'    => '',
			'select8'    => '',
			'select9'    => '',
			'select10'   => '',
			'select11'   => '',
			'select12'   => '',
			'select13'   => '',
			'select14'   => '',
			'select15'   => '',
		);

		extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

		<?php // Widget Title ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Widget Title', 'cas_theme' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		
		<?php 
			$options = array();
			// query for your post type
			$post_type_query  = new WP_Query(  
				array ( 
				    'post_type'=> 'post',
                    'post_status' => 'publish', 
					'posts_per_page' => -1,
					'tax_query' => array(
				        array(
				            'taxonomy' => 'post_format',
				            'field' => 'slug',
				            'terms' => array( 'post-format-quote', 'post-format-link' ),
				            'operator' => 'NOT IN'
				        )
				    )  
				)  
			);   
			// we need the array of posts
			$posts_array      = $post_type_query->posts;   
			// create a list with needed information
			// the key equals the ID, the value is the post_title
			$options = wp_list_pluck( $posts_array, 'post_title', 'ID' );
			wp_reset_postdata();
		?>
		<small>You can choose only among standard and video posts</small>
		<p>
			<label for="<?php echo $this->get_field_id( 'select' ); ?>"><?php _e( 'Select 1st Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select' ); ?>" id="<?php echo $this->get_field_id( 'select' ); ?>" class="widefat">
			<?php
	            echo '<option value="" '. selected( $select, '', false ) . '>--</option>';
				foreach ( $options as $key => $name ) {
					echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select, $key, false ) . '>'. $name . '</option>';
				} 
			?>
			</select>	
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'select2' ); ?>"><?php _e( 'Select 2nd Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select2' ); ?>" id="<?php echo $this->get_field_id( 'select2' ); ?>" class="widefat">
			<?php
            echo '<option value="" '. selected( $select2, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select2, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'select3' ); ?>"><?php _e( 'Select 3rd Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select3' ); ?>" id="<?php echo $this->get_field_id( 'select3' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select3, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select3, $key, false ) . '>'. $name . '</option>';
				
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select4' ); ?>"><?php _e( 'Select 4th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select4' ); ?>" id="<?php echo $this->get_field_id( 'select4' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select4, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select4, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select5' ); ?>"><?php _e( 'Select 5th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select5' ); ?>" id="<?php echo $this->get_field_id( 'select5' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select5, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select5, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select6' ); ?>"><?php _e( 'Select 6th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select6' ); ?>" id="<?php echo $this->get_field_id( 'select6' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select6, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select6, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select7' ); ?>"><?php _e( 'Select 7th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select7' ); ?>" id="<?php echo $this->get_field_id( 'select7' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select7, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select7, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select8' ); ?>"><?php _e( 'Select 8th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select8' ); ?>" id="<?php echo $this->get_field_id( 'select8' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select8, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select8, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select9' ); ?>"><?php _e( 'Select 9th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select9' ); ?>" id="<?php echo $this->get_field_id( 'select9' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select9, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select9, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select10' ); ?>"><?php _e( 'Select 10th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select10' ); ?>" id="<?php echo $this->get_field_id( 'select10' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select10, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select10, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select11' ); ?>"><?php _e( 'Select 11th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select11' ); ?>" id="<?php echo $this->get_field_id( 'select11' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select11, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select11, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select12' ); ?>"><?php _e( 'Select 12th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select12' ); ?>" id="<?php echo $this->get_field_id( 'select12' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select12, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select12, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select13' ); ?>"><?php _e( 'Select 13th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select13' ); ?>" id="<?php echo $this->get_field_id( 'select13' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select13, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select13, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select14' ); ?>"><?php _e( 'Select 14th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select14' ); ?>" id="<?php echo $this->get_field_id( 'select14' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select14, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select14, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'select15' ); ?>"><?php _e( 'Select 15th Casino', 'cas_theme' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select15' ); ?>" id="<?php echo $this->get_field_id( 'select15' ); ?>" class="widefat">
			<?php
			echo '<option value="" '. selected( $select15, '', false ) . '>--</option>';
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select15, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>	
		</p>

	<?php }
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		$instance['select']   = isset( $new_instance['select'] ) ? wp_strip_all_tags( $new_instance['select'] ) : '';
		$instance['select2']   = isset( $new_instance['select2'] ) ? wp_strip_all_tags( $new_instance['select2'] ) : '';
		$instance['select3']   = isset( $new_instance['select3'] ) ? wp_strip_all_tags( $new_instance['select3'] ) : '';
		$instance['select4']   = isset( $new_instance['select4'] ) ? wp_strip_all_tags( $new_instance['select4'] ) : '';
		$instance['select5']   = isset( $new_instance['select5'] ) ? wp_strip_all_tags( $new_instance['select5'] ) : '';
		$instance['select6']   = isset( $new_instance['select6'] ) ? wp_strip_all_tags( $new_instance['select6'] ) : '';
		$instance['select7']   = isset( $new_instance['select7'] ) ? wp_strip_all_tags( $new_instance['select7'] ) : '';
		$instance['select8']   = isset( $new_instance['select8'] ) ? wp_strip_all_tags( $new_instance['select8'] ) : '';
		$instance['select9']   = isset( $new_instance['select9'] ) ? wp_strip_all_tags( $new_instance['select9'] ) : '';
		$instance['select10']   = isset( $new_instance['select10'] ) ? wp_strip_all_tags( $new_instance['select10'] ) : '';
		$instance['select11']   = isset( $new_instance['select11'] ) ? wp_strip_all_tags( $new_instance['select11'] ) : '';
		$instance['select12']   = isset( $new_instance['select12'] ) ? wp_strip_all_tags( $new_instance['select12'] ) : '';
		$instance['select13']   = isset( $new_instance['select13'] ) ? wp_strip_all_tags( $new_instance['select13'] ) : '';
		$instance['select14']   = isset( $new_instance['select14'] ) ? wp_strip_all_tags( $new_instance['select14'] ) : '';
		$instance['select15']   = isset( $new_instance['select15'] ) ? wp_strip_all_tags( $new_instance['select15'] ) : '';
		return $instance;
	}
	public function print_best_item($id, $icon){
		if ( $id ) {
			echo '<li'.(!has_post_thumbnail($id)? ' class="flex-column justify-content-start"':'' ).'>';
			echo $icon;
			if(has_post_thumbnail($id)){
				echo '<figure class="widget_cas_img_cnt_outer"><a href="'. get_permalink($id) .'" class="widget_cas_img_cnt">'.
					   get_the_post_thumbnail( $id, 'small-thumbnail', ['class' => 'widget_cas_img_cnt__img', 'title' => get_the_title($id), 'alt' => get_the_title($id)] );
				echo '</a>';
                    if(function_exists('the_ratings')) { 
						echo '<span class="ratings_cnt position-absolute">';
						echo '<span class="post-ratings">';
						echo do_shortcode('[ratings id="'.$id.'" results="true"]');
						echo '</span>';
						echo '</span>';
					}
				echo '</figure>';
            }
			echo '<a href="'. get_permalink($id) .'" title="'. get_the_title($id).'" class="widget_title '.(!has_post_thumbnail($id)? 'align-self-start pl-0 pb_widget_link': '').'">'. get_the_title($id) .'</a>';
			if(!has_post_thumbnail($id)){
				if(function_exists('the_ratings')) { 
					echo '<span class="ratings_cnt position-relative">';
					echo '<span class="post-ratings">';
					echo do_shortcode('[ratings id="'.$id.'" results="true"]');
					echo '</span>';
					echo '</span>';
				}
			}
			
		}

	}
	public function widget( $args, $instance ) {
		extract( $args );
		$title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
		$select   = isset( $instance['select'] ) ? $instance['select'] : '';
		$select2   = isset( $instance['select2'] ) ? $instance['select2'] : '';
		$select3   = isset( $instance['select3'] ) ? $instance['select3'] : '';
		$select4   = isset( $instance['select4'] ) ? $instance['select4'] : '';
		$select5   = isset( $instance['select5'] ) ? $instance['select5'] : '';
		$select6   = isset( $instance['select6'] ) ? $instance['select6'] : '';
		$select7   = isset( $instance['select7'] ) ? $instance['select7'] : '';
		$select8   = isset( $instance['select8'] ) ? $instance['select8'] : '';
		$select9   = isset( $instance['select9'] ) ? $instance['select9'] : '';
		$select10   = isset( $instance['select10'] ) ? $instance['select10'] : '';
		$select11   = isset( $instance['select11'] ) ? $instance['select11'] : '';
		$select12   = isset( $instance['select12'] ) ? $instance['select12'] : '';
		$select13   = isset( $instance['select13'] ) ? $instance['select13'] : '';
		$select14   = isset( $instance['select14'] ) ? $instance['select14'] : '';
		$select15   = isset( $instance['select15'] ) ? $instance['select15'] : '';

		$svg_icons = array( 1 => '<span class="svg_widget_number">
					<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
					  <title>1</title>
					    <g transform="matrix(1.25 0 0 -1.25 0 45)">
							<g>
								<g>
									<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
									c13.3,0,24-10.7,24-24V-349.6z"/>
								</g>
							</g>
						</g>
						<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M250.9,158.7h-26c-21.1,0-29.9-15.4-29.9-30.4
						C195,113,206,98,225,98h62.5c18.9,0,29.5,13.6,29.5,31.2v250.4c0,22-14.1,34.3-33,34.3s-33-12.3-33-34.3V158.7H250.9z"/>
					</svg>
				</span>', '<span class="svg_widget_number">
								<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>2</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M262-249.1c15.4,0,27.3-7,27.3-23
												c0-16.1-11.9-23.1-25.1-23.1H147.5c-15.4,0-27.2,7-27.2,23.1c0,7.3,4.5,13.6,8,17.8c29,34.6,60.4,66,87,104.4
												c6.3,9.1,12.2,19.9,12.2,32.5c0,14.3-10.8,26.9-25.1,26.9c-40.2,0-21-56.6-54.5-56.6c-16.8,0-25.5,11.9-25.5,25.5
												c0,44,39.1,79.3,82.1,79.3S282-70.7,282-115c0-48.5-54.1-96.7-83.8-134.1H262L262-249.1z"/>
										</g>
									</g>
								</g>
								</svg>
							</span>', '<span class="svg_widget_number">
								<svg id="Layer_3" data-name="Layer 3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>3</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M275.6-105.3c0-21.6-9.3-40.5-28.2-52.6
												c24.7-11.3,41.9-34.4,41.9-61.5c0-41.2-37.8-75.9-86.6-75.9c-50.8,0-82.4,37.4-82.4,64.6c0,13.4,14.1,23,26.4,23
												c23.4,0,17.9-40.2,56.7-40.2c17.9,0,32.3,13.7,32.3,31.9c0,48.1-58.4,12.7-58.4,53.2c0,36.1,48.8,11.7,48.8,49.8
												c0,13.1-9.3,23-24.7,23c-32.6,0-28.2-33.7-51.5-33.7c-14.1,0-22.3,12.7-22.3,25.4c0,26.8,36.8,55.6,74.9,55.6
												C251.9-42.4,275.6-78.5,275.6-105.3"/>
										</g>
									</g>
								</g>
								</svg>
							</span>', '<span class="svg_widget_number">
								<svg id="Layer_4" data-name="Layer 4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>4</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M218.6-96.4H218L171.2-193h47.4V-96.4
												L218.6-96.4z M135.2-238.8c-17,0-24.2,11.4-24.2,20.4c0,7.6,2.8,11.8,4.9,15.2l77.6,140.6c7.6,13.8,17.3,20.1,35.3,20.1
												c20.1,0,39.8-12.8,39.8-44.3V-193h5.9c13.5,0,24.2-9,24.2-22.9s-10.7-22.9-24.2-22.9h-5.9v-29.4c0-18.4-7.3-27-24.9-27
												c-17.7,0-24.9,8.7-24.9,27v29.4L135.2-238.8L135.2-238.8z"/>
										</g>
									</g>
								</g>
								</svg>
							</span>', '<span class="svg_widget_number">
								<svg id="Layer_5" data-name="Layer 5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>5</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M191-127.6c8.4,1.7,17.5,3.5,26.2,3.5
												c42.3,0,72.3-33.5,72.3-74.4c0-58.3-32.1-96.7-92.5-96.7c-24.8,0-76.8,16.1-76.8,46.8c0,12.9,10.8,23.7,23.7,23.7
												c14,0,30.7-22.3,54.1-22.3c24.4,0,37,23.8,37,45.7c0,20.9-11.9,35.3-33.5,35.3c-20.3,0-23.4-11.5-41.2-11.5
												c-13.6,0-20.6,10.5-20.6,16.4c0,4.6,0.7,8,1,11.9l8.7,73.7c3.8,27.2,10.5,33.2,27.2,33.2h84.1c16.1,0,25.5-9.4,25.5-22
												c0-24.1-18.1-26.2-24.4-26.2h-65.6L191-127.6z"/>
										</g>
									</g>
								</g>
								</svg>
							</span>', '<span class="svg_widget_number">
								<svg id="Layer_6" data-name="Layer 6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>6</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M172.2-210c0-19.2,12-35.7,32.6-35.7
												c19.6,0,32.6,15.8,32.6,35.7c0,18.2-11,35.7-32.6,35.7C185.2-174.3,172.2-189.8,172.2-210 M118.6-210c0,51.9,52.6,124.7,87.2,157
												c2.8,2.7,6.9,5.5,11,8.2c3.8,2.4,7.6,2.4,10.3,2.4c9.6,0,24.4-12.7,24.4-25.1c0-4.8-3.1-8.9-7.2-14.1
												c-13.7-16.8-36.1-38.8-47.4-55l0.7-0.7c6.2,2.7,14.1,4.1,21.7,4.1c41.2,0,71.8-36.4,71.8-76.9c0-44.6-35.4-85.2-86.2-85.2
												C151.9-295.2,118.6-255.7,118.6-210"/>
										</g>
									</g>
								</g>
								</svg>
							</span>', '<span class="svg_widget_number">
								<svg id="Layer_7" data-name="Layer 7" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>7</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M231.5-91h-90.1c-19,0-26.1,12.3-26.1,24.3
												c0,12.3,6.7,24.3,26.1,24.3h127.5c18,0,25.4-16.2,25.4-25.4c0-7-3.9-14.4-8.1-23.2l-87.3-177.5c-9.9-19.7-13.7-26.8-29.6-26.8
												c-19.4,0-29.2,14.8-29.2,25c0,4.2,1.1,7.4,3.5,12.3L231.5-91z"/>
										</g>
									</g>
								</g>
								</svg>
							</span>', '<span class="svg_widget_number">
								<svg id="Layer_8" data-name="Layer 8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>8</title>
								 <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M180.1-113.8c0-15.5,8.9-26.1,24.7-26.1
												s25.1,10.6,25.1,26.1c0,14.8-8.9,26.1-25.1,26.1C188.7-87.7,180.1-99.1,180.1-113.8 M173.5-213.4c0-17.5,10.3-34.4,31.3-34.4
												c19.6,0,31.3,16.8,31.3,34.4c0,21.6-12.4,34.3-31.3,34.3C184.9-179.1,173.5-194.2,173.5-213.4 M120-216.9
												c0,27.5,15.1,47.7,36.8,59.4c-16.1,11.7-26.1,27.5-26.1,49.1c0,40.2,33.3,65.9,74.2,65.9c39.8,0,74.6-25.4,74.6-65.9
												c0-19.2-9.6-38.5-26.5-49.1c23.4-11.7,36.8-33,36.8-59.4c0-47.7-38.8-78.3-84.8-78.3C157.4-295.2,120-262.9,120-216.9"/>
										</g>
									</g>
								</g>
								</svg>
							</span>','<span class="svg_widget_number">
								<svg id="Layer_9" data-name="Layer 9" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>9</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M237.4-127.6c0,19.2-12,35.7-32.6,35.7
												c-19.6,0-32.6-15.8-32.6-35.7c0-18.2,11-35.7,32.6-35.7C224.4-163.3,237.4-147.8,237.4-127.6 M291-127.6
												c0-51.9-52.5-124.7-87.2-157c-2.7-2.7-6.9-5.5-11-8.2c-3.8-2.4-7.6-2.4-10.3-2.4c-9.6,0-24.4,12.7-24.4,25.1
												c0,4.8,3.1,8.9,7.2,14.1c13.7,16.8,36.1,38.8,47.4,55l-0.7,0.7c-6.2-2.7-14.1-4.1-21.6-4.1c-41.2,0-71.8,36.4-71.8,76.9
												c0,44.7,35.4,85.2,86.2,85.2C257.7-42.4,291-81.9,291-127.6"/>
										</g>
									</g>
								</g>
								</svg>
							</span>', '<span class="svg_widget_number svg_widget_number--two_digits">
								<svg id="Layer_10" data-name="Layer 10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>10</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M302.3-168.8c0,26.1-2.4,79.7-37.4,79.7
												c-35.1,0-37.5-53.6-37.5-79.7c0-24.3,2.4-79.7,37.5-79.7C299.9-248.5,302.3-193.1,302.3-168.8"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M264.9-295.2c-69.4,0-91.1,70.4-91.1,126.4"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M356-168.8c0-56-21.6-126.4-91.1-126.4"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M264.9-42.4c69.4,0,91.1-70.4,91.1-126.4"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M173.9-168.8c0,56,21.6,126.4,91.1,126.4"/>
										</g>
									</g>
								</g>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M121.4,161.3H96.2c-20.6,0-29.2-15-29.2-29.6
									c0-15,10.7-29.6,29.2-29.6H157c18.4,0,28.7,13.3,28.7,30.4v243.9c0,21.4-13.7,33.4-32.2,33.4c-18.4,0-32.1-12-32.1-33.4V161.3z"/>
								</svg>
							</span>', '<span class="svg_widget_number svg_widget_number--two_digits">
								<svg id="Layer_11" data-name="Layer 10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>11</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
										</g>
									</g>
								</g>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M176.4,158.7h-26c-21.1,0-29.9-15.4-29.9-30.4
									c0-15.4,11-30.4,29.9-30.4h62.5c18.9,0,29.5,13.6,29.5,31.2v250.4c0,22-14.1,34.3-33,34.3s-33-12.3-33-34.3V158.7z"/>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M325.5,158.7h-26c-21.1,0-29.9-15.4-29.9-30.4
									c0-15.4,11-30.4,29.9-30.4H362c18.9,0,29.5,13.6,29.5,31.2v250.4c0,22-14.1,34.3-33,34.3s-33-12.3-33-34.3V158.7z"/>
								</svg>
							</span>', '<span class="svg_widget_number svg_widget_number--two_digits">
								<svg id="Layer_12" data-name="Layer 12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>12</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
										</g>
									</g>
								</g>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M401.3,356.4c19.2,0,34,8.7,34,28.8
									S420.5,414,403.9,414H258.2c-19.2,0-34-8.7-34-28.8c0-9.1,5.6-17,10-22.3c36.2-43.1,75.5-82.4,108.6-130.5
									c7.8-11.4,15.2-24.8,15.2-40.6c0-17.9-13.6-33.6-31.5-33.6c-50.2,0-26.2,70.6-68.1,70.6c-21,0-31.9-14.8-31.9-31.9
									c0-55,48.9-99,102.5-99s96.9,35.3,96.9,90.7c0,60.6-67.6,120.9-104.7,167.5L401.3,356.4L401.3,356.4z"/>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M131.7,160h-25.6c-20.8,0-29.4-15.1-29.4-29.9
									c0-15.1,10.9-29.9,29.4-29.9h61.6c18.6,0,29.1,13.5,29.1,30.8v246.7c0,21.6-14,33.9-32.5,33.9s-32.5-12.1-32.5-33.9L131.7,160
									L131.7,160z"/>
								</svg>
							</span>', '<span class="svg_widget_number svg_widget_number--two_digits">
								<svg id="Layer_13" data-name="Layer 13" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>13</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M102.8-91.9H82.1c-16.8,0-23.8,12.2-23.8,24.2
												c0,12.2,8.8,24.2,23.8,24.2h49.8c15,0,23.5-10.9,23.5-24.9v-199.5c0-17.5-11.3-27.4-26.3-27.4s-26.3,9.8-26.3,27.4L102.8-91.9
												L102.8-91.9z"/>
										</g>
									</g>
								</g>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M422,176.3c0,27-11.6,50.5-35.1,65.4
									c30.8,14.1,52.2,42.7,52.2,76.6c0,51.3-47,94.5-107.7,94.5c-63.2,0-102.6-46.6-102.6-80.4c0-16.6,17.5-28.7,33-28.7
									c29.1,0,22.2,50,70.5,50c22.2,0,40.2-17.1,40.2-39.7c0-59.8-72.7-15.8-72.7-66.3c0-44.9,60.7-14.5,60.7-61.9
									c0-16.2-11.6-28.7-30.8-28.7c-40.6,0-35.1,41.9-64.1,41.9c-17.5,0-27.8-15.8-27.8-31.7c0-33.4,45.7-69.3,93.2-69.3
									C392.5,98,422,142.9,422,176.3"/>
								</svg>
							</span>', '<span class="svg_widget_number svg_widget_number--two_digits">
								<svg id="Layer_14" data-name="Layer 14" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>14</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
											<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M92.6-91.3H71.8C54.9-91.3,47.9-79,47.9-67
												c0,12.3,8.8,24.3,23.9,24.3h50c15.1,0,23.6-10.9,23.6-25V-268c0-17.6-11.3-27.5-26.4-27.5s-26.4,9.8-26.4,27.5
												C92.6-268,92.6-91.3,92.6-91.3z"/>
										</g>
									</g>
								</g>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M352.2,165.2h-0.9L292.9,286h59.3L352.2,165.2
									L352.2,165.2z M247.9,343.1c-21.2,0-30.3-14.3-30.3-25.5c0-9.5,3.5-14.7,6.1-19l97-175.8c9.5-17.3,21.6-25.1,44.1-25.1
									c25.1,0,49.8,16,49.8,55.4V286h7.4c16.9,0,30.3,11.3,30.3,28.6c0,17.3-13.4,28.6-30.3,28.6h-7.4V380c0,22.9-9.1,33.8-31.2,33.8
									S352.2,403,352.2,380v-36.8H247.9V343.1z"/>
								</svg>
							</span>', '<span class="svg_widget_number svg_widget_number--two_digits">
								<svg id="Layer_15" data-name="Layer 15" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
								  <title>15</title>
								  <g transform="matrix(1.25 0 0 -1.25 0 45)">
									<g>
										<g>
											<path style="fill:#B0DB43;" d="M409.6-349.6c0-13.3-10.7-24-24-24H24c-13.3,0-24,10.7-24,24V12c0,13.3,10.7,24,24,24h361.6
				c13.3,0,24-10.7,24-24V-349.6z"/>
										</g>
									</g>
								</g>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M314.9,204.6c10.5-2.2,21.8-4.4,32.7-4.4
									c52.8,0,90.2,41.9,90.2,92.9c0,72.9-40.1,120.8-115.6,120.8c-31,0-95.9-20.1-95.9-58.5c0-16.1,13.5-29.7,29.7-29.7
									c17.5,0,38.4,27.9,67.7,27.9c30.5,0,46.3-29.7,46.3-57.2c0-26.2-14.9-44.1-41.9-44.1c-25.3,0-29.3,14.4-51.5,14.4
									c-17,0-25.7-13.1-25.7-20.5c0-5.7,0.9-10,1.3-14.9l10.9-92c4.8-34,13.1-41.5,34-41.5h105.1c20.1,0,31.9,11.8,31.9,27.5
									c0,30.1-22.7,32.7-30.5,32.7h-82L314.9,204.6z"/>
								<path style="fill:none;stroke:#333333;stroke-width:14;stroke-miterlimit:10;" d="M130,158.9h-26c-21.1,0-29.9-15.4-29.9-30.4
									c0-15.4,11-30.4,29.9-30.4h62.5c18.9,0,29.5,13.6,29.5,31.2v250.2c0,22-14.1,34.4-33,34.4s-33-12.2-33-34.4
									C130,379.6,130,158.9,130,158.9z"/>
								</svg>
							</span>');
		// WordPress core before_widget hook (always include )
		echo '
			'. $before_widget;
			// Display the widget
			echo '<div class="best_cas">';

					if ( $title ) {
						echo  $before_title . '<div class="widget_title_cnt position-relative"><h4 class="widget_title_cnt__title">' . $title . '</h4></div>'. $after_title;
					}
					echo'<ul class="best_cas__list">';

					$this->print_best_item($select, $svg_icons[1]);
					$this->print_best_item($select2, $svg_icons[2]);
					$this->print_best_item($select3, $svg_icons[3]);
					$this->print_best_item($select4, $svg_icons[4]);
					$this->print_best_item($select5, $svg_icons[5]);
					$this->print_best_item($select6, $svg_icons[6]);
					$this->print_best_item($select7, $svg_icons[7]);
					$this->print_best_item($select8, $svg_icons[8]);
					$this->print_best_item($select9, $svg_icons[9]);
					$this->print_best_item($select10, $svg_icons[10]);
					$this->print_best_item($select11, $svg_icons[11]);
					$this->print_best_item($select12, $svg_icons[12]);
					$this->print_best_item($select13, $svg_icons[13]);
					$this->print_best_item($select14, $svg_icons[14]);
					$this->print_best_item($select15, $svg_icons[15]);
			
					echo'
					</ul>';
				echo '
				</div>';
			// WordPress core after_widget hook (always include )
			echo '
			' . $after_widget . '
	';
	}
}
function select_best_casino_in_widget() {
	register_widget( 'Select_Best_Cas_in_Widget' );
}
add_action( 'widgets_init', 'select_best_casino_in_widget' );
