<?php  
	function one_col_customizer_register($wp_customize){
		$wp_customize->add_section('site_logo',array(
			'title'				=> __('Site logo', 'cas_theme'),
			'description'		=> sprintf(__('Site logo', 'cas_theme')),
			'priority'			=> 10
		));
		$wp_customize-> add_setting('logo', 
			array(
				'default'		=> _x( get_template_directory_uri() . '/inc/images/logo.png', 'cas_theme'),
				'type' 			=> 'theme_mod',
				'capability'    => 'edit_theme_options',
				'transport'     => 'refresh',
			));
		$wp_customize-> add_control(new WP_Customize_Cropped_Image_Control( $wp_customize, 'logo', array(
				'label'        => __( 'Header site logo (120x120)', 'cas_theme' ),
				'section'      => 'site_logo',
				'settings'     => 'logo',
				'flex_width'   => true, // Optional. Default: false
                'flex_height'  => true,
				'width'        => 140,
                'height'       => 140,
			)
		));
		$wp_customize-> add_section('footer_social',array(
			'title'				=> __('Footer social icons', 'cas_theme'),
			'description'		=> sprintf(__('Footer social icons options', 'cas_theme')),
			'priority'			=> 210
		));
		$wp_customize-> add_setting('footer_social_one', array(
			'default'		=> _x('https://www.facebook.com/manchesterunited/', 'cas_theme'),
			'type' 			=> 'theme_mod'
		));
		$wp_customize-> add_control('footer_social_one', array(
			'label'			=> __('facebook link', 'cas_theme'),
			'section'		=> 'footer_social',
			'priority' 		=> 1
		));
		$wp_customize-> add_setting('footer_social_two', array(
			'default'		=> _x('https://www.facebook.com/mancity/', 'cas_theme'),
			'type' 			=> 'theme_mod'
		));
		$wp_customize-> add_control('footer_social_two', array(
			'label'			=> __('twitter link', 'cas_theme'),
			'section'		=> 'footer_social',
			'priority' 		=> 2
		));
	}
	add_action('customize_register', 'one_col_customizer_register');