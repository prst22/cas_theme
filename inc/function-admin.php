<?php
    function oc_add_admin_page() {
	
		//Generate Contact Admin Page
		add_menu_page( 'One Column Theme Options', 'Contact form', 'manage_options', 'one_column_theme_contact', 'oc_contact_form_page', 'dashicons-format-chat', 110 );

	}
	add_action( 'admin_menu', 'oc_add_admin_page' );

	//Activate custom settings
	

	function oc_custom_settings() {

		//Contact Form Options
		register_setting( 'oc-contact-options', 'activate_contact' );
		
		add_settings_section( 'test-contact-section', '', 'oc_contact_section', 'one_column_theme_contact');
		
		add_settings_field( 'activate-form', 'Activate Contact Form', 'oc_activate_contact', 'one_column_theme_contact', 'test-contact-section' );
	}

    add_action( 'admin_init', 'oc_custom_settings' );
	// admin contact form on/off page
	function oc_contact_form_page() {
		require_once( get_template_directory() . '/inc/templates/test-admin.php' );
	}

	function oc_contact_section() {
		echo 'Activate Contact Form';
	}

	function oc_activate_contact() {
		$options = get_option( 'activate_contact' );
		$checked = ( @$options == 1 ? 'checked' : '' );
		echo '<label><input type="checkbox" id="activate_contact" name="activate_contact" value="1" '. $checked .' /></label>';
	}
