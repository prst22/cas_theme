<?php
	add_action('wp_ajax_nopriv_request_posts', 'request_posts');
	add_action('wp_ajax_request_posts', 'request_posts');
	function request_posts(){
		$num = wp_strip_all_tags( $_POST["number"] );
		$num_p = ( $num == (int)$num ) ? (int)$num : 1;
		$search = wp_strip_all_tags($_POST["search"]);
		$categories = array_diff_key($_POST, ["number"=>"1", "search"=>"1", "action" =>"1"]);
		$ref_cat = array();
		foreach ($categories as $value) {
			array_push($ref_cat, wp_strip_all_tags($value));
		}
		$page_trail = get_site_url(null , null , 'relative') .'/';
		
        if($search !== '0'){
        	set_query_var('search_fetch_true', true);
        	$searhQuery = new WP_Query(
	        	array(
	        		'post_type'   => array('post', 'streams'),
	        		'post_status' => 'publish',
	        		'paged'       => $num_p,
			        's'           => $search
	        	)
	        );

	        if($searhQuery->have_posts()):
	        	echo '<div data-number="'. $num_p .'" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/page/'. $num_p .'/?s='. $search .'&submit=' . '">';
		        while($searhQuery->have_posts()): $searhQuery->the_post(); 
		        	get_template_part( 'template-parts/index', 'posts' );
				endwhile;
				if($searhQuery->max_num_pages > 1):
					echo '<div class="pagination_cnt pagination_cnt--search">';
						echo '<div class="pagination_cnt__inner" data-url="'. admin_url('admin-ajax.php') .'" data-search="'. $search .'">';
							echo paginate_links(array(
								'base'               => '#',
								'format'             => '',
								'prev_next'          => false,
								'type'               => 'plain',
								'total'              => $searhQuery->max_num_pages,
								'current'            => max( 1, $num_p ),
								'end_size'           => 2,
								'mid_size'           => 8
							));
						echo '</div>';
					echo '</div>';
				endif;
					echo '</div>';
				else:
					echo '<div data-number="'. $num_p .'" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/page/'. $num_p .'/?s='. $search .'&submit=' . '">';
						echo '<h1 class="text-center mt-5 mb-3 no_results_found">'. esc_html__( "No results found", "cas_theme" ) .'</h1>';
					echo '</div>';
				wp_reset_postdata();	
			endif;

        }else{
        	$pagination_num_ajx = (is_active_sidebar('side_panel')) ? 4 : 8;
        	set_query_var('fetch_pages', true);
        	set_query_var('cat_arr', $ref_cat);
        	$cat_url = implode (',', $ref_cat);
        	$nosearch_args = array(
        		'post_type'   => 'post',
        		'post_status' => 'publish',
        		'paged'       => $num_p,
        		'category__in'=> $ref_cat
        	);
        	$query_cus = new WP_Query($nosearch_args);
	        if($query_cus->have_posts() && !empty($categories)):
	        	echo '<div data-number="'. $num_p .'" class="post_item" data-page="' . $page_trail . 'page/'. $num_p .'">';
	            $_SESSION['cat_var_arr'] = $ref_cat;
		        while($query_cus->have_posts()): $query_cus->the_post(); 
		        	get_template_part( 'template-parts/index', 'posts' );
		        	
				endwhile;
				

				if($query_cus->max_num_pages > 1):
					echo '<div class="pagination_cnt">';
					echo '<div class="pagination_cnt__inner" data-url="'. admin_url('admin-ajax.php') .'">';
						echo paginate_links(array(
							'base'               => '#',
							'format'             => '',
							'prev_next'          => false,
							'type'               => 'plain',
							'total'              => $query_cus->max_num_pages,
							'current'            => max( 1, $num_p ),
							'end_size'           => 2,
							'mid_size'           => $pagination_num_ajx
						));
					echo '</div>';
					echo '</div>';
			    endif;
					echo '</div>';
                else:
                	$_SESSION['cat_var_arr'] = $ref_cat;
	                echo '<div data-number="'. $num_p .'" class="post_item" data-page="' . $page_trail . 'page/'. $num_p . '">'; 
                	echo '<h1 class="text-center mt-5 mb-3 no_results_found">'. esc_html__( "No results found", "cas_theme" ) .'</h1>';
                	echo '</div>';

                wp_reset_postdata();
			endif;
        }
		wp_die();
	}

	function if_paged($num = null){
		$output = '';
		if(is_paged()){ $output = 'page/'. get_query_var('paged');}

		if($num == 1){
			$num_p = ( get_query_var('paged') == 0 ? 1 : get_query_var('paged') );
            return $num_p;
		}else{
			return $output;
		}
	}

	// save contact form values

	add_action('wp_ajax_nopriv_cas_save_contact', 'cas_save_contact');
	add_action('wp_ajax_cas_save_contact', 'cas_save_contact');


	function cas_save_contact(){
	    $title = wp_strip_all_tags($_POST['name']);
	    $email = wp_strip_all_tags($_POST['email']);
	    $message = wp_strip_all_tags($_POST['message']);
	    $args = array(
	        'post_title' => $title,
	        'post_content' => $message,
	        'post_author' => 1,
	        'post_status' => 'publish',
	        'post_type' => 'oc-contact',
	        'meta_input' => array(
	            '_contact_email_value_key' => $email,
	        ),
	    );
	    $postID = wp_insert_post($args);

	    if($postID !== 0){
	    	$to = get_bloginfo('admin_email');
	    	$subject  = 'One Column Contact Form - '. $title;

	    	$headers[] = 'From: ' . get_bloginfo('name') . '<'. $to .'>';
	    	$headers[] = 'Reply-To: '. $title . '<'. $email .'>';
	    	$headers[] = 'Content-Type: text/html; chartset=UTF-8';

	    	wp_mail($to, $subject, $message, $headers);
	    	echo $postID;
	    }else{
	    	echo 0;
	    }
	    
	    wp_die();
	}

    add_action('wp_ajax_nopriv_cas_request_video_iframe', 'cas_request_video_iframe');
	add_action('wp_ajax_cas_request_video_iframe', 'cas_request_video_iframe');
	
	function cas_request_video_iframe(){
		$video_post_id = wp_strip_all_tags( $_POST["id"] );
		$video_query = new WP_Query(
	        	array(
	        		'post_type' => 'post',
					'posts_per_page' => -1,
	        		'post_status' => 'publish',
	        		'tax_query' => array( 
			        		array(
					            'taxonomy' => 'post_format',
					            'field' => 'slug',
					            'terms' => 'post-format-video',
				            )
		        		),
					'update_post_meta_cache' => false, 
					'update_post_term_cache' => false 
	        	)
	        );
	        
	        if($video_query->have_posts()):
	        	while($video_query->have_posts()): $video_query->the_post();
	        		if( (int)$video_post_id == get_the_ID()){
	        			$video_iframe_arr = get_media_embedded_in_content( apply_filters( 'the_content', get_the_content(), (int)$video_post_id));
	        		}
	        	endwhile;
	        	wp_reset_postdata();
	        endif; 
	        if(!empty($video_iframe_arr)){
	        	echo '<h3><a href="' . get_the_permalink((int)$video_post_id) . '">' . get_the_title((int)$video_post_id) . '</a></h3>';
		        echo '<div class="video_content">' . $video_iframe_arr[0] . '</div>';
		    }else{
		    	echo '<h3 class="text-center mt-5 mb-3 no_results_found">' . esc_html__('Failed to load video', 'cas_theme') . '</h3>';
		    }
		wp_die();
	}