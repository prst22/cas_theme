<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<title>
		<?php bloginfo('name');?> |
		<?php is_front_page() ? bloginfo('description') : wp_title('');?> 
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Igor Barabash">
	<meta name="keywords" content="Simple wordpress theme">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<style>
		.nav { padding: 10px 0; display: none; }
		.nav a { font-size: 1.5rem; -webkit-transition: color 0.2s cubic-bezier(0.445, 0.05, 0.55, 0.95); -o-transition: color 0.2s cubic-bezier(0.445, 0.05, 0.55, 0.95); transition: color 0.2s cubic-bezier(0.445, 0.05, 0.55, 0.95); font-weight: bold; text-decoration: none; padding: 0 10px; line-height: 36px; }
		.nav a:link { color: #6c757d; }
		.nav a:visited { color: #6c757d; }
		.nav a:hover { color: #B0DB43; }
		.nav__inner { background: -webkit-radial-gradient(center, ellipse, #c5ff32 0%, #b0db43 100%); background: -o-radial-gradient(center, ellipse, #c5ff32 0%, #b0db43 100%); background: radial-gradient(ellipse at center, #c5ff32 0%, #b0db43 100%); padding: 9px 0.5rem; padding-top: 0; margin-right: 14px; }
		.widget_title_cnt { background: -webkit-radial-gradient(center, ellipse, #c5ff32 0%, #b0db43 100%); background: -o-radial-gradient(center, ellipse, #c5ff32 0%, #b0db43 100%); background: radial-gradient(ellipse at center, #c5ff32 0%, #b0db43 100%); padding: 9px 0.5rem; padding-top: 0; -webkit-box-shadow: 0px 1px 3px #777; box-shadow: 0px 1px 3px #777; }
		.widget_title_cnt__title{ text-align: center; color: #b0db43; padding: 0.5em; }
		.nav_list, .widget_title_cnt__title { position: relative; width: calc(100% + 22px * 2); left: -22px; background: url(<?php echo get_template_directory_uri() .'/inc/images/bg.png'; ?>); background-color: #333333; -webkit-box-shadow: 0px 1px 3px #212529; box-shadow: 0px 1px 3px #212529; }
		.nav_list::before, .widget_title_cnt__title::before { display: block; content: ""; width: 0; height: 0; border-style: solid; border-width: 0 14px 9px 0; border-color: transparent #212529 transparent; position: absolute; left: 0; bottom: -9px; }
		.nav_list::after, .widget_title_cnt__title::after { display: block; content: ""; width: 0; height: 0; border-style: solid; border-width: 9px 14px 0 0; border-color: #212529 transparent transparent transparent; position: absolute; right: 0; bottom: -9px; }
		.nav_list { padding-top: 5px; }
		.text-center { text-align: center; }
		.site_wrap { position: relative; background-color: #f8f9fa; }
		.top { background: url(<?php echo get_template_directory_uri() .'/inc/images/bg.png'; ?>); background-color: #2F2F2F; position: relative; -webkit-box-shadow: 0px 1px 3px #777; box-shadow: 0px 1px 3px #777; }
		.top.border{ border-bottom: 4px solid #4E4E4E; -webkit-box-shadow: none; box-shadow: none;  }
		.top__logo { display: none; }
		.top .mid_logo a { display: block; margin: 64px; margin-left: 0; margin-bottom: 0; width: 100px; }
		.top .mid_logo .logo_margin { margin-bottom: 64px; }
		.site_header_cnt { width: 100%; }
		.site_header_cnt__site_header > a > img { max-height: 80px; width: auto; }
		.main_site_header { font-weight: bold; padding-top: 50px; padding-bottom: 10px; }
		.main_site_header > a:link { color: #6c757d; }
		.main_site_header > a:visited { color: #6c757d; }
		.main_site_header > a:hover { color: #B0DB43; }
		.main { position: relative; overflow: hidden; -webkit-transition: transform 0.5s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; -o-transition: transform 0.5s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; transition: transform 0.5s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; will-change: transform; }
		.main .rocbot { font-weight: bold; opacity: 0; -webkit-transition: opacity 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; -o-transition: opacity 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; transition: opacity 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; text-transform: uppercase; text-align: center; position: absolute; bottom: -66px; left: 0; right: 0; line-height: 24px; }
		.main .rocbot.reveal { opacity: 1; }
		/* filter block start*/
		.filter_cnt { position: relative; }
		.filter_cnt .btn{font-size: 1.5rem;}
		.filter_cnt .min { min-height: 0; }
		.filter_cnt .show { max-height: 700px; -webkit-animation-name: rollin; animation-name: rollin; -webkit-animation-duration: 0.4s; animation-duration: 0.4s; -webkit-animation-fill-mode: forwards; animation-fill-mode: forwards; }
		.filter_cnt .hidden { max-height: 0px; -webkit-animation-name: rollout; animation-name: rollout; -webkit-animation-duration: 0.4s; animation-duration: 0.4s; -webkit-animation-fill-mode: forwards; animation-fill-mode: forwards; }
		.filter_cnt #sort { white-space: nowrap; }
		.filter_cnt .sort_show { color: #B0DB43; vertical-align: middle; }
		.filter_cnt__button { position: absolute; z-index: 100; left: -20px; bottom: -34px; border: 4px solid #7999a9; cursor: pointer; height: 34px; padding: 8px 10px 5px 10px; background-color: #e9ecef; outline: none; text-transform: uppercase; color: #2F2F2F; font-weight: bold; vertical-align: middle; -webkit-transition: color 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; -o-transition: color 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; transition: color 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; -webkit-box-shadow: 0px 1px 3px #777; box-shadow: 0px 1px 3px #777; font-family: 'Josefin Sans', sans-serif; border-radius: 2px; font-size: 13px; line-height: 13px;}
		.filter_cnt__button .filter_button_icon { color: #B0DB43; position: absolute; right: -3.2rem; top: 0; vertical-align: text-top; font-size: 3.2rem; }
		.filter_cnt__button::focus { outline: none; }
		.filter_cnt__button::active { outline: none; }
		.filter_cnt__button:hover { color: #e83e8c; }
		.sorting_post_cnt { color: #fff; overflow: hidden; -webkit-transition: max-height 0.35s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; -o-transition: max-height 0.35s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; transition: max-height 0.35s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; -webkit-transform-origin: 0% 0%; -ms-transform-origin: 0% 0%; transform-origin: 0% 0%; -webkit-box-shadow: 0 1px 3px #777; box-shadow: 0 1px 3px #777; -webkit-transform: translateZ(0); transform: translateZ(0); background: url(<?php echo get_template_directory_uri() .'/inc/images/bg.png'; ?>); background-color: #2F2F2F; position: relative; }
		.sorting_post_cnt .sorting_lable, .sorting_post_cnt .cat_title { text-transform: uppercase; }
		.sorting_post_cnt .sorting_lable { font-size: 14px; }
		.sorting_post_cnt__item { -webkit-box-flex: 0; -ms-flex: 0 1 300px; flex: 0 1 300px; padding-top: calc(20px + 4px); padding-bottom: calc(20px + 4px); }
		.sorting_post_cnt__item .cat_title { width: 100%; padding-bottom: 20px; }
		.sorting_post_cnt__item .category_field { -webkit-box-flex: 0; -ms-flex: 0 1 270px; flex: 0 1 270px; }
		.category_field input[type="checkbox"]{width: 20px; height: 20px;}
		.sorting_post_cnt__item:last-of-type { -webkit-box-flex: 1; -ms-flex: 1 1 50%; flex: 1 1 50%; }
		.sorting_post_cnt__item:first-of-type { padding-left: 20px; padding-right: 20px; }
		.sorting_post_cnt:after { content: ''; display: block; border-bottom: 4px solid #B0DB43; border-top: 1px solid #777; width: 100%; position: absolute; bottom: 0; left: 0; right: 0; top: 0; z-index: -1; }


		.single_title { font-size: 1.5rem; padding: 1em 0 1em; }
		.post-navigation .nav-links { display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-pack: justify; -ms-flex-pack: justify; justify-content: space-between; }
		.post-navigation .nav-links .icon { font-size: 24px; vertical-align: middle; }
		.post-navigation .nav-links .nav-previous, .post-navigation .nav-links .nav-next { -webkit-transition: box-shadow 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; -o-transition: box-shadow 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; transition: box-shadow 0.3s cubic-bezier(0.445, 0.05, 0.55, 0.95) 0s; background: url(<?php echo get_template_directory_uri() .'/inc/images/bg.png'; ?>); background-color: #2F2F2F; border: 1px solid #6c757d; -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 1px 6px rgba(0, 0, 0, 0.23); box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 1px 6px rgba(0, 0, 0, 0.23); border-radius: 2px; }
		.post-navigation .nav-links .nav-previous:hover, .post-navigation .nav-links .nav-next:hover { -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.4), 0 1px 6px rgba(0, 0, 0, 0.23); box-shadow: 0 3px 6px rgba(0, 0, 0, 0.4), 0 1px 6px rgba(0, 0, 0, 0.23); }
		.post-navigation .nav-links .nav-previous > a, .post-navigation .nav-links .nav-next > a { width: 120px; height: 50px; margin: 0; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-align: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; }
		.post-navigation .nav-links .nav-previous { margin-right: 0.5rem; }
		.post-navigation .nav-links .nav-next { margin-left: 0.5rem; }
		.post-navigation .nav-links div:first-of-type { margin-left: 0; }
		.home .main { min-height: 1374px; }
		.single_post_main, .main { min-height: calc(100vh - (225px + 210px)); }
		.single-post .main{overflow: initial;}
		.single_post_main__inner{overflow: hidden;}
		.d_bg{background-color: #262626;}
		.posts_cnt__loader_cnt{ display: none; }
	</style>
	<?php wp_head(); ?>
</head>
<body <?php (is_singular('streams') ? body_class('d_bg') : body_class()); ?>>
	<section class="d-flex justify-content-center align-items-center search_block hidden_search" id="search_block">
		<div class="search_block__close_search_cnt">
			<i class="cross" id="close_search">
				<svg class="icon icon-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-close"></use>
				</svg>
			</i>
		</div>
		<div class="search_block_inner search_animate">
	    	<?php get_search_form(); ?>
    	</div>	
	</section>
	<div class="tingle-content-wrapper"><!--tingle/search wrapper fpr blur effect on video modal -->
	<?php if ( has_nav_menu( 'main_menu' ) ) : ?>
	<section class="mobile_menu_wrapper">

		<nav class="mobile_menu_cnt hidden">
			<div class="mobile_menu_cnt__inner column_hidden">
				<div class="menu_content">
	                <div class="close_menu_cnt">
						<i class="cross" id="close_menu">
							<svg class="icon icon-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-close"></use>
							</svg>
						</i>
	                </div>
					<?php
					   wp_nav_menu( array(
						   'menu'              => 'Main menu',
						   'theme_location'    => 'main_menu',
						   'menu_class'        => 'mobile_menu_list_cnt__mobile_list',
						   'depth'             => 1, 
						   'container_class'   => 'mobile_menu_list_cnt',                  
						   )
					   );
					?>
				</div>
			</div>
		</nav>

	</section> 
	<?php endif; ?>
	
	<div class="site_wrap">
		<section class="<?php echo (is_singular('streams') ? 'top border' : 'top'); ?>">
			<div class="container">
				<div class="row">
					<div class="d-none d-lg-block col-lg-2 mid_logo ">
						<?php if (get_theme_mod('logo') !== ''): ?>
							<a href="<?php echo home_url();?>" alt="Home" <?php echo ( !has_nav_menu( 'main_menu' ) ? 'class="logo_margin"':''); ?>>
								<img src="<?php  echo (( get_theme_mod( 'logo' ) > 0 ) ? wp_get_attachment_url( get_theme_mod( 'logo' ) ) : esc_url(get_template_directory_uri() . '/inc/images/logo.png')); ?>">
							</a>
						<?php endif; ?>
					</div>
					<div class="d-flex align-items-center justify-content-center justify-content-lg-end col-lg-10 site_header_cnt">
						<header class="d-flex justify-content-center justify-content-lg-end align-items-center site_header_cnt__site_header">
							<h1 class="main_site_header">
								<a href="<?php echo home_url();?>" title="Go home">
									<?php bloginfo('name'); ?>
								</a>
							</h1>
						</header>
					</div>
					<?php if ( has_nav_menu( 'main_menu' ) ) : ?>
					<div class="col-12">
                        <div class="mobile_menu_toggle">
						    <div class="mobile_menu_toggle__button" id="mobile_menu_btn">
								<span></span>
								<span></span>
						    </div>
						</div>

						<nav class="d-lg-flex justify-content-end align-items-center nav">
							
							<?php
							   wp_nav_menu( array(
								   'menu'              => 'Main menu',
								   'theme_location'    => 'main_menu',
								   'menu_class'        => 'd-md-inline-flex justify-content-center nav_list',
								   'depth'             => 1,          
								   'container_class'   => 'nav__inner position-relative',           
								   )
							   );
							?>
						</nav>

					</div>
					<?php endif; ?>
				</div>
			</div>
			<?php if (get_theme_mod('logo') !== ''): ?>
			<div class="top__logo box_shadow box_shadow--logo" <?php echo ((has_nav_menu( 'main_menu' ))?: 'style="bottom:-5.5rem;"'); ?>>
				
				<a href="<?php echo home_url();?>">
					<img src="<?php  echo (( get_theme_mod( 'logo' ) > 0 ) ? wp_get_attachment_url( get_theme_mod( 'logo' ) ) : esc_url(get_template_directory_uri() . '/inc/images/logo.png')); ?>" alt="<?php bloginfo('name');?>">
				</a>
				
                <div class="logo_lable">
                	<a class="logo_lable__link" href="http://www.potatosites.com/">www.potatosites.com</a>	
                </div>

			</div>
			<?php endif; ?>
		</section>
	
	
	