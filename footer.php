	<div class="container-fluid">
		<div class="row">
			<footer class="col-12 footer">
				<div class="footer__inner">
					<?php  if( is_active_sidebar('footer_widget') || has_nav_menu( 'footer_menu' ) ): ?>

						<div class="my-3 d-flex justify-content-center justify-content-sm-between align-items-center flex-column flex-sm-row footer_content">
							<div class="container-fluid">
								<div class="row">
									<div class="d-flex justify-content-center justify-content-lg-start align-items-center col-12 col-lg-5 p-0">
										<?php if ( has_nav_menu( 'footer_menu' ) ) : ?>
											<nav class="footer_content__footer_menu">
												<?php
												$args = array(
													'theme_location' => 'footer_menu'
												);
												wp_nav_menu($args); ?>

											</nav>
										<?php endif; ?>
									</div>
									<div class="d-flex justify-content-center align-items-center col-12 col-lg-2 p-0">
										<?php 
										$icon_facebook = get_theme_mod('footer_social_one');
										$icon_twitter = get_theme_mod('footer_social_two');
										if (!empty($icon_facebook) || !empty($icon_twitter)): ?>
				                            <div class="footer_content__social">
											<?php if (get_theme_mod('footer_social_one') !== ''): ?>
												<a href="<?php echo get_theme_mod('footer_social_one'); ?>" title="Our facebook link">
													<i>
														<svg class="icon icon-facebook-official"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-facebook-official"></use></svg>
													</i>
												</a>
											<?php endif; ?>
											<?php if(get_theme_mod('footer_social_two') !== ''): ?>
												<a href="<?php echo get_theme_mod('footer_social_two'); ?>" title="Our twitter link">
													<i>
														<svg class="icon icon-twitter"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-twitter"></use></svg>
													</i>
												</a>
											<?php endif; ?>
				                            </div>
			                            <?php endif; ?>
									</div>
									<div class="d-flex justify-content-center justify-content-lg-end align-items-center col-12 col-lg-5 p-0">
										<?php
											if(is_active_sidebar('footer_widget')){ 
									            dynamic_sidebar('footer_widget');
								            }
										?>
									</div>
								</div>
							</div>

                        </div>
                    <?php endif; ?>

					<div class="about" id="footer">
						<h4 class="about__header">Terms</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat.</p>	
					</div>
                    
		        </div>

				<div class="footer__bottom-copy">
					<span class="footer_copy_icon">
						<svg class="icon icon-copyright"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-copyright"></use></svg>
					</span>
					
					<?php echo date("Y") ;?> - <a href="http://www.potatosites.com/">potatosites.com</a>	
				</div> 
				
			</footer>
		</div>
	</div>
	
</div>
</div><!--tingle/search wrapper fpr blur effect on video modal end -->
<div class="go_top">
	<span class="go_top__icon hidden_g">
		<svg class="icon icon-level-up"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-level-up"></use></svg>
	</span>
</div>
<?php wp_footer(); ?>
</body>

</html>
