<?php
/*
Template Name: page 404
*/
?>
<?php
get_header(); ?>

<main class="container main">
    <div class="row">
        <div class="col-12">
        	<div class="page_404 d-flex justify-content-center align-items-center">
        		<h1>Something went wrong 404</h1>
        	</div>
		</div>
	</div>
</main>

<?php get_footer();